# Generated by Django 2.1.5 on 2019-06-09 23:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0004_auto_20190610_0550'),
    ]

    operations = [
        migrations.AddField(
            model_name='agendaactivity',
            name='best_time_score',
            field=models.BigIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='agendaactivity',
            name='best_time_start',
            field=models.PositiveIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='agendaactivity',
            name='best_time_until',
            field=models.PositiveIntegerField(default=1),
            preserve_default=False,
        ),
    ]
