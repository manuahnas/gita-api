from datetime import timedelta

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.utils.dateparse import parse_datetime

from core_api.abstracts.base import BaseAbstractModel
from social_network.models import GroupProfile, LocationProfile
from .agendas import AgendaActivity

User = get_user_model()


class Activities(BaseAbstractModel):
    """
    Could be Activity of an user or many user
    """
    duration = models.BigIntegerField(blank=True)
    time_start = models.DateTimeField()
    time_until = models.DateTimeField(blank=True)
    activity_name = models.CharField(max_length=64)
    description = models.TextField(max_length=255, default='', blank=True)
    group = models.ForeignKey(GroupProfile, on_delete=models.SET_NULL, null=True, blank=True)
    location = models.ForeignKey(LocationProfile, on_delete=models.SET_NULL, null=True, blank=True)
    # agenda = models.ForeignKey(
    #     Agenda, on_delete=models.CASCADE, blank=True, null=True,
    #     related_name='posted_activities'
    # )
    agenda_activity = models.ForeignKey(
        AgendaActivity, on_delete=models.CASCADE, blank=True, null=True,
        related_name='generated_activities'
    )

    class Meta:
        ordering = ('-time_start',)

    def __str__(self):
        return '{} at {} until {}'.format(self.activity_name, self.time_start, self.time_until)

    def save(self, *args, **kwargs):
        if not bool(self.duration) and not bool(self.time_until):
            raise ValidationError(
                'Should provide at least one between valid duration or time_until. Default lookup would be duration.'
            )
        if isinstance(self.time_start, str):
            self.time_start = parse_datetime(self.time_start)
        if isinstance(self.time_until, str):
            self.time_until = parse_datetime(self.time_until)

        if self.duration:
            self.time_until = self.time_start + timedelta(seconds=self.duration)
        else:
            self.duration = self.time_until.timestamp() - self.time_start.timestamp()

        if self.time_start > self.time_until:
            raise ValidationError(
                'time_until must be greater than time_start.'
            )
        super().save(*args, **kwargs)


# class UserPersonalRangedActivity(BaseAbstractModel):
#     user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='activities')
#     activity = models.CharField(max_length=64)
#     description = models.TextField(max_length=255, default='')
#     duration = models.BigIntegerField()
#     agenda_start = models.DateTimeField()
#     agenda_until = models.DateTimeField()


class UserActivities(BaseAbstractModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='activities')
    activity = models.ForeignKey(Activities, on_delete=models.CASCADE, related_name='user_activities')
    is_activity_valid = models.BooleanField(default=True, blank=True)

    def __str__(self):
        return '{} with {} activity: {}'.format(
            self.user, 'valid' if self.is_activity_valid else 'invalid', self.activity)

    def save(self, *args, **kwargs):
        self.is_activity_valid = self.check_is_activity_valid()
        super().save(*args, **kwargs)

    def check_is_activity_valid(self):
        q = UserActivities.objects.filter(user=self.user)
        q_objects = Q(
            activity__time_start__lte=self.activity.time_start,
            activity__time_until__gte=self.activity.time_start
        )
        q_objects |= Q(
            activity__time_start__lte=self.activity.time_until,
            activity__time_until__gte=self.activity.time_until
        )
        q_objects |= Q(
            activity__time_start__gte=self.activity.time_start,
            activity__time_until__lte=self.activity.time_until
        )
        return not q.filter(q_objects).exists()


class InvitationActivities(BaseAbstractModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='activity_invites')
    activity = models.ForeignKey(Activities, on_delete=models.CASCADE)
    rejected = models.DateTimeField(blank=True, null=True)
