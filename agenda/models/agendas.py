from datetime import datetime

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.utils.translation import ugettext_lazy as _

from core_api.abstracts.base import BaseAbstractModel
from core_api.tasks import generate_agenda_task
from core_api.utils.agenda import reformat_time
from social_network.models import GroupProfile, LocationProfile

User = get_user_model()


class Agenda(BaseAbstractModel):
    NO_LOOP = 0
    SECONDLY = 1
    MINUTELY = 2
    HOURLY = 3
    DAILY = 4
    WEEKLY = 5
    MONTHLY = 6
    YEARLY = 7
    LOOP_TYPE = (
        (NO_LOOP, _('Once')),
        (SECONDLY, _('Secondly')),
        (MINUTELY, _('Minutely')),
        (HOURLY, _('Hourly')),
        (DAILY, _('Daily')),
        (WEEKLY, _('Weekly')),
        (MONTHLY, _('Monthly')),
        (YEARLY, _('Yearly')),
    )

    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='agenda_created')

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_agendas', null=True)
    group = models.ForeignKey(GroupProfile, on_delete=models.CASCADE, related_name='group_agendas', null=True)

    agenda_name = models.CharField(max_length=64)
    description = models.TextField(max_length=255, default='', blank=True)

    loop = models.SmallIntegerField(choices=LOOP_TYPE, default=NO_LOOP)
    loop_interlude = models.PositiveIntegerField(default=0, blank=True)
    range_time_start = models.DateTimeField()
    range_time_until = models.DateTimeField(blank=True)
    loop_count = models.PositiveIntegerField(default=0, blank=True)

    is_valid = models.BooleanField(default=False, blank=True)
    published = models.DateTimeField(blank=True, null=True)
    is_in_progress = models.BooleanField(default=False, blank=True)

    class Meta:
        ordering = ('-created_at',)

    @property
    def type(self):
        return [self.loop, self.loop_interlude]

    @property
    def time(self):
        return [self.loop_count, self.range_time_start.timestamp(), self.range_time_until.timestamp()]

    def generate_activity(self, delay=True):
        if delay:
            generate_agenda_task.delay(self.id)
        else:
            generate_agenda_task(self.id)

    def save(self, *args, **kwargs):
        if bool(self.user) == bool(self.group):
            raise ValidationError('Should provide one between user or group.')

        if self.loop == self.NO_LOOP:
            self.loop_count = 0
        else:
            if not self.pk:
                if bool(self.range_time_until) == bool(self.loop_count):
                    raise ValidationError('Should provide one between valid range_time_until or loop_count.')

                if isinstance(self.range_time_start, str):
                    self.range_time_start = parse_datetime(self.range_time_start)
                timestamp_start = self.range_time_start.timestamp()
                if self.range_time_until:
                    if isinstance(self.range_time_until, str):
                        self.range_time_until = parse_datetime(self.range_time_until)
                    timestamp_until = self.range_time_until.timestamp()
                else:
                    timestamp_until = 0

                count, start, until = reformat_time(
                    [self.loop, self.loop_interlude],
                    [self.loop_count, timestamp_start, timestamp_until]
                )
                if self.range_time_until:
                    self.loop_count = count
                else:
                    default_timezone = timezone.get_default_timezone()
                    self.range_time_until = datetime.fromtimestamp(until, default_timezone)

            if self.range_time_start > self.range_time_until:
                raise ValidationError(
                    'range_time_until must be greater than range_time_start.'
                )
        super().save(*args, **kwargs)


class AgendaLocationCandidate(BaseAbstractModel):
    agenda = models.ForeignKey(Agenda, on_delete=models.CASCADE, related_name='locations')
    location = models.ForeignKey(LocationProfile, on_delete=models.CASCADE, related_name='location_of_agendas')


class AgendaActivity(BaseAbstractModel):
    agenda = models.ForeignKey(Agenda, on_delete=models.CASCADE, related_name='activities')
    activity_name = models.CharField(max_length=64)
    description = models.TextField(max_length=255, blank=True, default='')
    duration = models.PositiveIntegerField()
    location = models.ForeignKey(
        LocationProfile, on_delete=models.CASCADE, null=True, default=None,
        related_name='candidate_location_of_agendas'
    )
    best_time_start = models.PositiveIntegerField(null=True, default=None)
    best_time_until = models.PositiveIntegerField(null=True, default=None)
    best_time_score = models.BigIntegerField(null=True, default=None)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.agenda.user:
            AgendaHost.objects.get_or_create(agenda_activity=self, host=self.agenda.user)
            AgendaMember.objects.get_or_create(agenda_activity=self, member=self.agenda.user)


class AgendaHost(BaseAbstractModel):
    agenda_activity = models.ForeignKey(AgendaActivity, on_delete=models.CASCADE, related_name='hosts')
    host = models.ForeignKey(User, on_delete=models.CASCADE, related_name='host_of_agendas')


class AgendaMember(BaseAbstractModel):
    agenda_activity = models.ForeignKey(AgendaActivity, on_delete=models.CASCADE, related_name='members')
    member = models.ForeignKey(User, on_delete=models.CASCADE, related_name='member_of_agendas')
