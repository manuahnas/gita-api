from rest_framework import serializers

from agenda.models.activities import Activities, UserActivities


class ActivitiesSerializer(serializers.ModelSerializer):
    # time_until = serializers.SerializerMethodField()
    #
    # def get_time_until(self, obj):
    #     return obj.time_until

    class Meta:
        model = Activities
        fields = (
            'id',
            'duration',
            'time_start',
            'time_until',
            'activity_name',
            'description',

            'group',
            'location',
            'agenda_activity',
            'created_at',
            'updated_at',
        )
        read_only_fields = (
            'id',
            'group',
            'location',
            'agenda_activity',
            'time_until',
            'created_at',
            'updated_at',
        )


class UserActivitiesSerializer(serializers.ModelSerializer):
    activity = ActivitiesSerializer(many=False)

    class Meta:
        model = UserActivities
        fields = ('id', 'activity', 'is_activity_valid')
        read_only_fields = ('is_activity_valid',)

    def create(self, validated_data):
        activity_data = validated_data.pop('activity')
        activity = Activities.objects.create(
            time_start=activity_data['time_start'],
            duration=activity_data['duration'],
            activity_name=activity_data['activity_name'],
            description=activity_data['description'],
        )
        return UserActivities.objects.create(
            user=self.context['request'].user,
            activity=activity
        )

    def update(self, instance, validated_data):
        activity_data = validated_data.pop('activity')
        user_activity = instance.activity
        user_activity.time_start = activity_data.get('time_start', user_activity.time_start)
        user_activity.duration = activity_data.get('duration', user_activity.duration)
        user_activity.activity_name = activity_data.get('activity_name', user_activity.activity_name)
        user_activity.description = activity_data.get('description', user_activity.description)
        user_activity.save()
        return instance


class UserActivityCheckSerializer(serializers.ModelSerializer):
    except_id = serializers.IntegerField(required=False)
    activity = ActivitiesSerializer(many=False)

    class Meta:
        model = UserActivities
        fields = ('id', 'activity', 'is_activity_valid', 'except_id',)
        read_only_fields = ('id', 'is_activity_valid',)
