from django.contrib.auth import get_user_model
from rest_framework import serializers

from agenda.models import (
    Agenda, AgendaLocationCandidate, AgendaActivity, AgendaHost, AgendaMember,
)

User = get_user_model()


class AgendaHostSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgendaHost
        fields = ('host',)


class AgendaMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgendaMember
        fields = ('member',)


class AgendaActivitySerializer(serializers.ModelSerializer):
    hosts = AgendaHostSerializer(many=True, required=False)
    members = AgendaMemberSerializer(many=True, required=False)

    # def to_representation(self, instance: AgendaActivity):
    #     data = super().to_representation(instance)
    #     data['hosts'] = AgendaHostSerializer(instance.hosts.all(), many=True)
    #     data['members'] = AgendaHostSerializer(instance.members.all(), many=True)
    #     return data

    class Meta:
        model = AgendaActivity
        fields = (
            'activity_name',
            'description',
            'duration',
            'hosts',
            'members',
        )


class AgendaLocationCandidateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgendaLocationCandidate
        fields = ('location',)


class AgendaSerializer(serializers.ModelSerializer):
    locations = AgendaLocationCandidateSerializer(many=True)
    activities = AgendaActivitySerializer(many=True)

    class Meta:
        model = Agenda
        fields = (
            'id',
            'agenda_name',
            'description',
            'loop',
            'loop_interlude',
            'range_time_start',
            'range_time_until',
            'loop_count',

            'locations',
            'activities',

            'is_valid',
            'published',
            'is_in_progress',
        )
        read_only_fields = (
            'id',
            'is_valid',
            'published',
            'is_in_progress',
        )

    def create(self, validated_data):
        locations_data = validated_data.pop('locations')
        activities_data = validated_data.pop('activities')

        agenda = Agenda.objects.create(
            creator=self.context['request'].user,
            user=validated_data.get('user', None),
            group=validated_data.get('group', None),
            agenda_name=validated_data['agenda_name'],
            description=validated_data.get('description', ''),
            loop=validated_data.get('loop', Agenda.NO_LOOP),
            loop_interlude=validated_data.get('loop_interlude', 0),
            range_time_start=validated_data['range_time_start'],
            range_time_until=validated_data.get('range_time_until', None),
            loop_count=validated_data.get('loop_count', 0),
        )

        AgendaLocationCandidate.objects.bulk_create([
            AgendaLocationCandidate(
                agenda=agenda, location=location_data['location']
            ) for location_data in locations_data
        ])

        for activity_data in activities_data:
            hosts_data = activity_data.pop('hosts', [])
            members_data = activity_data.pop('members', [])
            agenda_activity = AgendaActivity.objects.create(
                agenda=agenda,
                activity_name=activity_data['activity_name'],
                description=activity_data.get('description', ''),
                duration=activity_data['duration'],
                location=activity_data.get('location', None),
            )

            AgendaHost.objects.bulk_create([
                AgendaHost(
                    agenda_activity=agenda_activity,
                    host=host_data['host']
                ) for host_data in hosts_data
            ])
            AgendaMember.objects.bulk_create([
                AgendaMember(
                    agenda_activity=agenda_activity,
                    member=member_data['member']
                ) for member_data in members_data
            ])
        return agenda

    def update(self, instance, validated_data):
        locations_data = validated_data.pop('locations')
        activities_data = validated_data.pop('activities')

        instance.user = validated_data.get('user', instance.user)
        instance.group = validated_data.get('group', instance.group)
        instance.agenda_name = validated_data.get('agenda_name', instance.agenda_name)
        instance.description = validated_data.get('description', instance.description)
        instance.loop = validated_data.get('loop', instance.loop)
        instance.loop_interlude = validated_data.get('loop_interlude', instance.loop_interlude)
        instance.range_time_start = validated_data.get('range_time_start', instance.range_time_start)
        instance.range_time_until = validated_data.get('range_time_until', instance.range_time_until)
        instance.loop_count = validated_data.get('loop_count', instance.loop_count)

        instance.locations.all().delete()
        AgendaLocationCandidate.objects.bulk_create([
            AgendaLocationCandidate(
                agenda=instance, location=location_data['location']
            ) for location_data in locations_data
        ])

        instance.activities.all().delete()
        for activity_data in activities_data:
            if activity_data.get('hosts', False):
                hosts_data = activity_data.pop('hosts')
            else:
                hosts_data = {'host': self.context['request'].user},

            if activity_data.get('members', False):
                members_data = activity_data.pop('members')
            else:
                members_data = {'member': self.context['request'].user},

            agenda_activity = AgendaActivity.objects.create(
                agenda=instance,
                activity_name=activity_data['activity_name'],
                description=activity_data.get('description', ''),
                duration=activity_data['duration'],
                location=activity_data.get('location', None),
            )

            AgendaHost.objects.bulk_create([
                AgendaHost(
                    agenda_activity=agenda_activity,
                    host=host_data['host']
                ) for host_data in hosts_data
            ])
            AgendaMember.objects.bulk_create([
                AgendaMember(
                    agenda_activity=agenda_activity,
                    member=member_data['member']
                ) for member_data in members_data
            ])
        return instance
