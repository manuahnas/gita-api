from django.urls import path

from agenda.views.agenda import (
    PersonalAgendaAPIView, PersonalAgendaDetailAPIView, PersonalAgendaGenerateAPIView,
    GroupAgendaAPIView, GroupAgendaDetailAPIView, GroupAgendaGenerateAPIView,
)
from agenda.views.activity import (
    PersonalActivityAPIView, PersonalActivityDetailAPIView, PersonalActivityCheckAPIView,
    GroupActivityAPIView, GroupActivityDetailAPIView, GroupActivityCheckAPIView,
)
from agenda.views.schedule import (
    UserScheduleAPIView,
    GroupScheduleAPIView,
    LocationScheduleAPIView,
)

urlpatterns = [
    path('', PersonalAgendaAPIView.as_view(), name='agenda-personal'),
    path('detail/<int:agenda_id>/', PersonalAgendaDetailAPIView.as_view(), name='agenda-detail_personal'),
    path('generate/<int:agenda_id>/', PersonalAgendaGenerateAPIView.as_view(), name='agenda-generate_personal'),
    path('group/<int:group_id>/', GroupAgendaAPIView.as_view(), name='agenda-group'),
    path('group/<int:group_id>/detail/<int:agenda_id>/', GroupAgendaDetailAPIView.as_view(),
         name='agenda-detail_group'),
    path('group/<int:group_id>/generate/<int:agenda_id>/', GroupAgendaGenerateAPIView.as_view(),
         name='agenda-generate_group'),

    path('activity/', PersonalActivityAPIView.as_view(), name='activity-personal'),
    path('activity/<int:activity_id>/', PersonalActivityDetailAPIView.as_view(), name='activity-detail_personal'),
    path('activity/check/', PersonalActivityCheckAPIView.as_view(), name='activity-check_personal'),
    path('activity/group/<int:group_id>/', GroupActivityAPIView.as_view(), name='activity-group'),
    path('activity/group/<int:group_id>/detail/<int:activity_id>/', GroupActivityDetailAPIView.as_view(),
         name='activity-detail_group'),
    path('activity/group/<int:group_id>/check/', GroupActivityCheckAPIView.as_view(), name='activity-check_group'),

    path('schedule/user/<int:user_id>/', UserScheduleAPIView.as_view(), name='schedule-user'),
    path('schedule/group/<int:group_id>/', GroupScheduleAPIView.as_view(), name='schedule-group'),
    path('schedule/location/<int:location_id>/', LocationScheduleAPIView.as_view(), name='schedule-location'),
]
