from django.apps import AppConfig


class AuditTrailsConfig(AppConfig):
    name = 'audit_trails'
