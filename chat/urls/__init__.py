from django.urls import path

from core_api.views.echo import (
    EchoPublicAPIView, EchoUserAPIView, EchoAdminAPIView,
)

urlpatterns = [
    path('public/', EchoPublicAPIView.as_view(), name='echo_public'),
    path('user/', EchoUserAPIView.as_view(), name='echo_user'),
    path('admin/', EchoAdminAPIView.as_view(), name='echo_admin'),
]
