from django.db import models
from django.utils.timezone import now

from core_api.managers import BaseManager


class BaseAbstractModel(models.Model):
    deleted = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    super_objects = models.Manager()
    objects = BaseManager()

    class Meta:
        abstract = True

    def soft_delete(self):
        self.deleted = now()
        super().save()
