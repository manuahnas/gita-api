from django.db import models

from .base import BaseAbstractModel


def user_directory_path(instance, filename):
    return 'upload/user_{0}/{1}'.format(instance.user.id, filename)


class AbstractPictures(BaseAbstractModel):
    profile_cover = models.ImageField(upload_to=user_directory_path)

    class Meta:
        abstract = True
