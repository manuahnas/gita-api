import unicodedata

from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .base import BaseAbstractModel


class UsernameAbstractModel(BaseAbstractModel):
    username_validator = UnicodeUsernameValidator()
    username = models.CharField(
        verbose_name=_('Username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("This username already used."),
        },
        null=True,
        default=None,
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.username

    def normalize_username(self):
        return unicodedata.normalize('NFKC', str(self.username)) if self.username else self.username

    def save(self, *args, **kwargs):
        self.username = self.normalize_username()
        super().save(*args, **kwargs)
