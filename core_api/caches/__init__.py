from django.core.cache import cache

CACHE_TYPES = {
    'friends': 'f-{}',
    'followers': 'fo-{}',
    'following': 'fl-{}',
    'requests': 'fr-{}',
    'sent_requests': 'sfr-{}',
    'unread_requests': 'fru-{}',
    'unread_request_count': 'fruc-{}',
    'read_requests': 'frr-{}',
    'rejected_requests': 'frj-{}',
    'unrejected_requests': 'frur-{}',
    'unrejected_request_count': 'frurc-{}',
    'groups': 'gr-{}',
    'administrated_groups': 'gradm-{}',
    'group_admins': 'gra-{}',
    'locations': 'loc-{}',
    'administrated_locations': 'locadm-{}',
    'location_admins': 'loca-{}',
}

BUST_CACHES = {
    'friends': ['friends'],
    'followers': ['followers'],
    'following': ['following'],
    'requests': [
        'requests',
        'unread_requests',
        'unread_request_count',
        'read_requests',
        'rejected_requests',
        'unrejected_requests',
        'unrejected_request_count',
    ],
    'sent_requests': ['sent_requests'],
    'groups': ['groups']
}


def cache_key(cache_type: str, pk):
    """
    Build the cache key for a particular type of cached value
    """
    return CACHE_TYPES[cache_type].format(pk)


def bust_cache(cache_bust, user_pk):
    """
    Bust our cache for a given type, can bust multiple caches
    """
    bust_keys = BUST_CACHES[cache_bust]
    keys = [CACHE_TYPES[key].format(user_pk) for key in bust_keys]
    cache.delete_many(keys)
