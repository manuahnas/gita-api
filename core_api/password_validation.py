from django.contrib.auth.password_validation import UserAttributeSimilarityValidator
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


class CustomUserAttributeSimilarityValidator(UserAttributeSimilarityValidator):
    """
    Validate whether the password is sufficiently different from the user's
    attributes.

    If no specific attributes are provided, look at a sensible list of
    defaults. Attributes that don't exist are ignored. Comparison is made to
    not only the full attribute value, but also its components, so that, for
    example, a password is validated against either part of an email address,
    as well as the full address.
    """
    DEFAULT_USER_ATTRIBUTES = ('username', 'first_name', 'last_name', 'email', 'phone',)

    def __init__(self, user_attributes=DEFAULT_USER_ATTRIBUTES, max_similarity=0.7):
        super().__init__(user_attributes, max_similarity)


class ContainNumericPasswordValidator:
    """
    Validate whether the password contain at least one numeric character.
    """

    def validate(self, password, user=None):
        if not any(char.isdigit() for char in password):
            raise ValidationError(
                _("This password does not contain any number."),
                code='password_entirely_numeric',
            )

    def get_help_text(self):
        return _("Your password should contain at least one number.")
