from rest_framework.permissions import BasePermission

from .posts import IsFeedOwnerAndAuthenticatedUser
from .profile import (
    IsUserOwnerAndAuthenticatedUser, IsFriendOfUserOwnerAndAuthenticatedUser, IsPublicAndAuthenticatedUser,
    IsProfileOwnerAndAuthenticatedUser,
)


class IsDefaultAuthenticatedUser(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        return True
