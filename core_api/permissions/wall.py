from django.contrib.auth import get_user_model
from rest_framework import permissions

from relationship.models import Friendship
from social_network.models import GroupProfile, LocationProfile, WallPost, GroupWallPost, LocationWallPost

User = get_user_model()


class IsFriendOfUserAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users and wall owner is friend.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: User):
        if obj.id == request.user.id:
            return True
        elif Friendship.objects.are_friends(obj, request.user):
            return True
        return False


class IsMemberOfGroupAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users and wall owner is joined group.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: GroupProfile):
        return obj.is_member(request.user)


class IsAdminOfLocationAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users and wall owner is joined location.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: LocationProfile):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.is_admin(request.user)


class IsOwnerWallPostAndFriendOfUserAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users and update delete for owner of wall post.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: WallPost):
        """
        if user_a and user_b are friend, user_a send wall post to user_b
        TODO: action if user_b block/delete friendship with user_a
        """
        if obj.wall_of_user.id == request.user.id:
            if obj.user.id == request.user.id:
                return True
            elif request.method in permissions.SAFE_METHODS + ['DELETE']:
                return True
            return False
        elif Friendship.objects.are_friends(obj.wall_of_user, request.user):
            if obj.user.id == request.user.id:
                return True
            elif request.method in permissions.SAFE_METHODS:
                return True
        return False


class IsOwnerWallPostAndMemberOfGroupAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users and update delete for owner of wall post.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: GroupWallPost):
        if obj.wall_of_group.is_member(request.user):
            if request.method in permissions.SAFE_METHODS:
                return True
            if obj.user.id == request.user.id:
                return True
            if obj.wall_of_group.is_admin(request.user) and request.method == 'DELETE':
                return True
        return False


class IsOwnerWallPostAndAdminOfLocationAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users and update delete for owner of wall post.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: LocationWallPost):
        if obj.wall_of_location.is_admin(request.user):
            if obj.user.id == request.user.id:
                return True
            elif request.method in permissions.SAFE_METHODS + ['DELETE']:
                return True
        elif request.method in permissions.SAFE_METHODS:
            return True
        return False
