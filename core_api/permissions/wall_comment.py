from rest_framework import permissions

from relationship.models import Friendship
from social_network.models import WallPostComment, GroupWallPostComment, LocationWallPostComment


class IsOwnerOrFriendOfWallPostCommentAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated user and user is wall owner or wall owner friend.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: WallPostComment):
        """
        if user_a and user_b are friend, user_a send wall post comment to user_b
        TODO: action if user_b block/delete friendship with user_a
        """
        if obj.wall_post.wall_of_user.id == request.user.id:
            if request.method in permissions.SAFE_METHODS + ['DELETE']:
                return True
            elif obj.user.id == request.user.id:
                return True
        elif Friendship.objects.are_friends(obj.wall_post.wall_of_user, request.user):
            if request.method in permissions.SAFE_METHODS:
                return True
            elif obj.user.id == request.user.id:
                return True
        return False


class IsOwnerOfWallPostCommentOrMemberOfGroupAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users
    and update delete for wall post comment owner
    and get for group members
    and get delete for group admins.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: GroupWallPostComment):
        if obj.wall_post.wall_of_group.is_member(request.user):
            if request.method in permissions.SAFE_METHODS:
                return True
            if obj.user.id == request.user.id:
                return True
            if obj.wall_post.wall_of_group.is_admin(request.user) and request.method == 'DELETE':
                return True
        return False


class IsOwnerWallPostCommentOrAdminOfLocationAndAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users
    and update delete for wall post comment owner
    and get for all user
    and get delete for location admins.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: LocationWallPostComment):
        if request.method in permissions.SAFE_METHODS:
            return True
        elif obj.wall_post.wall_of_location.is_admin(request.user):
            if request.method in permissions.SAFE_METHODS + ['DELETE']:
                return True
        elif obj.user.id == request.user.id:
            return True
        return False
