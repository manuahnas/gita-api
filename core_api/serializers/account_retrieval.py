from rest_framework import serializers


class RecoverySerializer(serializers.Serializer):
    username = serializers.CharField()


class ValidateRecoverySerializer(serializers.Serializer):
    token = serializers.CharField()
    uuid = serializers.UUIDField()
    is_valid = serializers.BooleanField(read_only=True)


class ResetPasswordSerializer(serializers.Serializer):
    token = serializers.CharField()
    uuid = serializers.UUIDField()
    password = serializers.CharField(write_only=True)
