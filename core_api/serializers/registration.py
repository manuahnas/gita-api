from django.contrib.auth import password_validation, get_user_model
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core import exceptions
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

GitaUser = get_user_model()


class CheckUsernameRegisterSerializer(serializers.Serializer):
    username = serializers.CharField()
    is_valid = serializers.BooleanField(read_only=True)


class UserRegisterSerializer(serializers.ModelSerializer):
    phone = PhoneNumberField(required=True)
    birthday = serializers.DateField(write_only=True, required=True)

    class Meta:
        model = GitaUser
        fields = ('phone', 'first_name', 'last_name', 'username', 'password', 'birthday',)
        extra_kwargs = {
            'password': {'write_only': True},
            'username': {'validators': []},
        }

    def validate_username(self, value):
        GitaUser.username_validator(value)
        return value

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if GitaUser.objects.filter(
                username=attrs['username'], phone_verified__isnull=False
        ).exclude(phone=attrs['phone']).exists():
            raise serializers.ValidationError(
                detail={'username': [_("Username already registered.")]},
                code="register_username_already_registered"
            )
        if GitaUser.objects.filter(phone=attrs['phone'], phone_verified__isnull=False).exists():
            raise serializers.ValidationError(
                detail={'phone': [_("Phone number already registered.")]},
                code="register_phone_already_registered"
            )

        try:
            user = GitaUser(
                phone=attrs['phone'], first_name=attrs['first_name'], last_name=attrs.get('last_name', ''),
                username=attrs['username']
            )
            password_validation.validate_password(password=attrs['password'], user=user)
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(
                detail=serializers.as_serializer_error(e), code="register_not_feasible_password"
            )
        return attrs

    def create(self, validated_data):
        if GitaUser.objects.filter(phone=validated_data['phone']).exists():
            if GitaUser.objects.filter(username=validated_data['username']).exists():
                user = GitaUser.objects.get(username=validated_data['username'])
                user.username = user.phone.as_national
                user.save()
            instance = GitaUser.objects.get(phone=validated_data['phone'])
            instance.profile.birthday = validated_data['birthday']
            instance.profile.save()
            instance.first_name = validated_data['first_name']
            instance.last_name = validated_data.get('last_name', '')
            instance.username = validated_data['username']
            instance.set_password(validated_data['password'])
            instance.save()
            return instance
        return GitaUser.objects.create_gita_user(
            validated_data['phone'],
            data_profile={'birthday': validated_data['birthday']},
            password=validated_data['password'],
            first_name=validated_data['first_name'],
            last_name=validated_data.get('last_name', ''),
            username=validated_data['username'],
        )


class ResendPhoneTokenSerializer(serializers.Serializer):
    phone = PhoneNumberField()


class VerifyPhoneSerializer(serializers.Serializer):
    phone = serializers.CharField(write_only=True)
    phone_token = serializers.CharField(write_only=True)
    phone_key = serializers.CharField(read_only=True)

    def validate_phone_token(self, value):
        from social_network.models.verification import UserPhoneVerificationToken
        if not UserPhoneVerificationToken.objects.filter(token=value).exists():
            msg = _("Invalid token.")
            raise ValidationError(msg, code='phone_verification_token_invalid')
        return value
