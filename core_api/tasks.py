from celery import shared_task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


@shared_task
def generate_agenda_task(agenda_id):
    from agenda.models import Agenda
    from gita_algo.gita import GiTa
    agenda = Agenda.objects.get(id=agenda_id)
    agenda.is_in_progress = True
    agenda.save()
    gita = GiTa(
        agenda=agenda
    )
    gita.run()
    try:
        gita.apply_result()
    except ValueError:
        agenda.is_valid = False
    else:
        agenda.is_valid = True
    agenda.is_in_progress = False
    agenda.save()
