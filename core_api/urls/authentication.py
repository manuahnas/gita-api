from django.urls import path

from core_api.views.authentication import (
    LoginAPIView, VerifyAPIView, LogoutAPIView
)

urlpatterns = [
    path('login/', LoginAPIView.as_view(), name='authentication-login'),
    path('verify/', VerifyAPIView.as_view(), name='authentication-verify'),
    path('logout/', LogoutAPIView.as_view(), name='authentication-logout'),

    # path('obtain-token/', ObtainJSONWebToken.as_view(), name='authentication-token_get'),
    # path('verify-token/', VerifyJSONWebToken.as_view(), name='authentication-token_verify'),
    # path('refresh-token/', RefreshJSONWebToken.as_view(), name='authentication-token_refresh'),
]
