from django.urls import path

from core_api.views.public import (
    FAQAPIView, TOSAPIView,
)

urlpatterns = [
    path('faq/', FAQAPIView.as_view(), name='faq'),
    path('tos/', TOSAPIView.as_view(), name='tos'),
]
