from django.urls import path

from core_api.views.registration import (
    CheckUsernameRegisterAPIView, UserRegisterAPIView, ResendPhoneTokenAPIView, VerifyPhoneRegisterAPIView
)

urlpatterns = [
    path('check-username/', CheckUsernameRegisterAPIView.as_view(), name='registration-check_username'),
    path('register/', UserRegisterAPIView.as_view(), name='registration-register'),
    path('resend-token/', ResendPhoneTokenAPIView.as_view(), name='registration-resend_token'),
    path('verify-phone/', VerifyPhoneRegisterAPIView.as_view(), name='registration-verify_phone'),
]
