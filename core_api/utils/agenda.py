from datetime import datetime, timedelta


# from dateutil import relativedelta


def reformat_time(_type, _time):
    """
    (sekali, tiap beberapa detik, menitan, jam2an, harian, mingguan, bulanan, tahunan)
    """
    if _type[0] == 1:
        # TODO:every second task
        return _time
    elif _type[0] == 2:
        # TODO: every minute task
        return _time
    elif _type[0] == 3:
        # formating hourly task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_hourly()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_hourly()
    elif _type[0] == 4:
        # formating daily task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_daily()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_daily()
    elif _type[0] == 5:
        # formating weekly task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_weekly()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_weekly()
    elif _type[0] == 6:
        # formating monthly task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_monthly()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_monthly()
    elif _type[0] == 7:
        # formating annualy task
        if _time[0]:
            the_repeat = Repeat(_time[1], interlude=_type[1], count=_time[0])
            return the_repeat.getuntil_annualy()
        else:
            the_repeat = Repeat(_time[1], interlude=_type[1], date_until=_time[2])
            return the_repeat.getcount_annualy()
    else:
        return _time


class Repeat:
    def __init__(self, date_start, interlude, date_until=0, count=0):
        self.date_start = date_start
        self.interlude = interlude
        self.date_until = date_until
        self.count = count

    def getuntil_hourly(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(hours=(self.interlude * self.count))
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_hourly(self):
        diff = datetime.fromtimestamp(self.date_until) - datetime.fromtimestamp(self.date_start)
        count = round(diff.seconds / (3600 * self.interlude))
        the_time = (count, self.date_start, self.date_until)
        return the_time

    def getuntil_daily(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(days=(self.interlude * self.count))
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_daily(self):
        diff = datetime.fromtimestamp(self.date_until) - datetime.fromtimestamp(self.date_start)
        count = round(diff.days / self.interlude)
        the_time = (count, self.date_start, self.date_until)
        return the_time

    def getuntil_weekly(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(weeks=(self.interlude * self.count))
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_weekly(self):
        diff = datetime.fromtimestamp(self.date_until) - datetime.fromtimestamp(self.date_start)
        count = round(diff.days / (7 * self.interlude))
        the_time = (count, self.date_start, self.date_until)
        return the_time

    def getuntil_monthly(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(
            days=(self.interlude * self.count * 30))  # relativedelta(months=self.interlude * self.count)
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_monthly(self):
        diff = datetime.fromtimestamp(self.date_until) - datetime.fromtimestamp(self.date_start)
        count = round(diff.days / (30 * self.interlude))
        the_time = (count, self.date_start, self.date_until)
        return the_time

    def getuntil_annualy(self):
        date_until = datetime.fromtimestamp(self.date_start) + timedelta(days=(self.interlude * self.count * 365))
        the_time = (self.count, self.date_start, date_until.timestamp())
        return the_time

    def getcount_annualy(self):
        diff = datetime.fromtimestamp(self.date_until) - datetime.fromtimestamp(self.date_start)
        count = round(diff.days / (365 * self.interlude))
        the_time = (count, self.date_start, self.date_until)
        return the_time


class Loop:
    def __init__(self, schedule):
        self.schedule = schedule

    def get_hourly(self):
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(datetime(1970, 1, 1, 8, minute=date.minute, second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)

    def get_daily(self):
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(datetime(1970, 1, 1, hour=date.hour, minute=date.minute, second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)

    def get_weekly(self, range):
        if range:
            return tuple([316800, 921599])
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(datetime(1970, 1, date.weekday() + 5, hour=date.hour, minute=date.minute,
                                        second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)

    def get_monthly(self):
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(
                    datetime(1970, 1, date.day, hour=date.hour, minute=date.minute, second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)

    def get_annualy(self):
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(datetime(1970, date.month, date.day, hour=date.hour, minute=date.minute,
                                        second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)


def time_to_type(schedule, _type, is_range=False):
    the_loop = Loop(schedule)
    if _type[0] == 1:
        # TODO:every second task
        if is_range:
            return schedule
        return schedule
    elif _type[0] == 2:
        # TODO: every minute task
        return schedule
    elif _type[0] == 3:
        # formating hourly task
        if is_range:
            return schedule
        return the_loop.get_hourly()
    elif _type[0] == 4:
        # formating daily task
        if is_range:
            return schedule
        return the_loop.get_daily()
    elif _type[0] == 5:
        # formating weekly task
        return the_loop.get_weekly(is_range)
    elif _type[0] == 6:
        # formating monthly task
        if is_range:
            return schedule
        return the_loop.get_monthly()
    elif _type[0] == 7:
        # formating annualy task
        if is_range:
            return schedule
        return the_loop.get_annualy()
    else:
        return schedule
