from rest_framework import permissions, generics, status
from rest_framework.response import Response

from core_api.serializers.echo import EchoSerializer


class EchoPublicAPIView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = EchoSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        response_data = {
            'message': serializer.validated_data['message'],
            'status': 'Echo public test success.'
        }
        return Response(response_data, status=status.HTTP_201_CREATED)


class EchoUserAPIView(generics.CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = EchoSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        response_data = {
            'message': serializer.validated_data['message'],
            'status': 'Echo user test success.'
        }
        return Response(response_data, status=status.HTTP_201_CREATED)


class EchoAdminAPIView(generics.CreateAPIView):
    permission_classes = (permissions.IsAdminUser,)
    serializer_class = EchoSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        response_data = {
            'message': serializer.validated_data['message'],
            'status': 'Echo admin test success.'
        }
        return Response(response_data, status=status.HTTP_201_CREATED)
