from django.contrib.auth import get_user_model
from django.utils.timezone import now

from agenda.models import Agenda
from gita_algo.ga import genetic_algorithm
from gita_algo.schedule import merge_activities
from gita_algo.utils import reformat_time_according_type, parse_time_according_type, datetime_according_type

# logging.basicConfig(stream=sys.stdout, level=logging.NOTSET)

User = get_user_model()


class GiTa:
    def __init__(self, agenda: Agenda):
        # logging.info("Initialization searching for agenda with ID: %s", agenda.id)
        self.agenda = agenda

        agenda_type = [agenda.loop, agenda.loop_interlude]
        # logging.info("Agenda type: %s", agenda_type)

        # one between loop_count or range_time_until could be blank, find it
        agenda_time = reformat_time_according_type(
            agenda_type,
            [agenda.loop_count, agenda.range_time_start.timestamp(), agenda.range_time_until.timestamp()]
        )

        # parse and "fold" time to make it fit with available schedule
        time_by_type = parse_time_according_type(
            agenda_time[1:], agenda_type, True
        )

        self.agenda_type = agenda_type
        self.agenda_time = (agenda_time[0], time_by_type[0], time_by_type[1])
        # logging.info(f"Agenda Time: "
        #              f"interlude {self.agenda_time[0]}, "
        #              f"start {self.agenda_time[1]}, "
        #              f"end {self.agenda_time[2]}")

        # if not agenda.locations.exists():
        #     self.location_schedule_map = {0: []}
        # else:
        #     self.location_schedule_map = {}
        #     for location_candidate in agenda.locations.all():
        #         location_schedule = location_candidate.location.get_close_time(self.agenda_type, self.agenda_time)
        #         self.location_schedule_map[location_candidate.id] = location_schedule

        self.results = [
            {
                'agenda_activity': activity,
                'best_location': activity.location if activity.location else None if agenda.locations.exists() else '*',
                'best_time': None,
                'best_time_score': None,
            } for activity in agenda.activities.all()
        ]

    def run(self):
        schedule = []
        for index, result in enumerate(self.results):
            if result['best_time'] and result['best_location']:
                print(
                    "Waktu dan tempat terbaik untuk aktifitas ini sudah ditemukan pada %s di lokasi dengan id %s.",
                    "Sehingga perhitungan untuk aktifitas ini tidak diperlukan.",
                    result['best_time'], result['best_location']
                )
                continue
            configuration = {
                'agenda_activity': result['agenda_activity'],
                'agenda_type': self.agenda_type,
                'agenda_time': self.agenda_time,
            }
            if result['agenda_activity'].location:
                schedule += result['agenda_activity'].location.get_close_time(self.agenda.type, self.agenda.time)
                algorithm_result = genetic_algorithm(
                    configuration,
                    schedule
                )
                self.results[index]['best_time'] = algorithm_result['best_time']
                self.results[index]['best_time_score'] = algorithm_result['best_time_score']
                self.results[index]['best_location'] = result['agenda_activity'].location.id
                continue
            for location_candidate in self.agenda.locations.all():
                schedule += location_candidate.location.get_close_time(self.agenda.type, self.agenda.time)
                algorithm_result = genetic_algorithm(
                    configuration,
                    schedule
                )
                if not algorithm_result['best_time_score']:
                    continue
                if self.results[index]['best_time_score'] \
                        and self.results[index]['best_time_score'] > algorithm_result['best_time_score']:
                    continue
                schedule.append(list(algorithm_result['best_time']))
                schedule = merge_activities(schedule)
                self.results[index]['best_time'] = algorithm_result['best_time']
                self.results[index]['best_time_score'] = algorithm_result['best_time_score']
                self.results[index]['best_location'] = location_candidate.location.id
        print("Pencarian selesai.")
        return self.results

    def apply_result(self):
        from agenda.models import Activities, UserActivities
        from social_network.models import LocationProfile
        if not all([result['best_time'] for result in self.results]):
            raise ValueError("Unable to find best time for this agenda.")
        for result in self.results:
            if result['agenda_activity'].agenda.id != self.agenda.id:
                continue
            result['agenda_activity'].best_time_start = result['best_time'][0]
            result['agenda_activity'].best_time_until = result['best_time'][1]
            result['agenda_activity'].best_time_score = result['best_time_score']
            result['agenda_activity'].save()

            activities = [
                Activities(
                    duration=result['agenda_activity'].duration,
                    time_start=parsed_time[0],
                    time_until=parsed_time[1],
                    activity_name=result['agenda_activity'].activity_name,
                    description=result['agenda_activity'].description,
                    group=result['agenda_activity'].agenda.group,
                    location=LocationProfile.objects.get(id=result['best_location'])
                    if result['best_location'] != '*' and result['best_location'] else None,
                    agenda_activity=result['agenda_activity'],
                ) for parsed_time in datetime_according_type(
                    result['best_time'], self.agenda.type, self.agenda.time
                )
            ]
            activities = Activities.objects.bulk_create(activities)

            hosts = User.objects.filter(host_of_agendas__agenda_activity=result['agenda_activity'])
            members = User.objects.filter(member_of_agendas__agenda_activity=result['agenda_activity'])
            activity_users = (hosts | members).distinct()
            user_activities = [
                UserActivities(user=user, activity=activity)
                for user in activity_users for activity in activities
            ]
            UserActivities.objects.bulk_create(user_activities)
        self.agenda.published = now()
        self.agenda.save()
