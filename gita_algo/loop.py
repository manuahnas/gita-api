from datetime import datetime


class Loop:
    def __init__(self, schedule):
        self.schedule = schedule

    def get_hourly(self):
        new_schedule = []
        for activities in self.schedule:
            new_activities = []
            for activity in activities:
                if activity[1] - activity[0] > 3600:
                    return (([
                                 int(datetime(1970, 1, 1, 8, minute=0, second=0).timestamp()),
                                 int(datetime(1970, 1, 1, 8, minute=59, second=59).timestamp()),
                             ],),)
                datetime_start = datetime.fromtimestamp(activity[0])
                datetime_until = datetime.fromtimestamp(activity[1])
                new_datetime_start = int(
                    datetime(1970, 1, 1, 8, minute=datetime_start.minute, second=datetime_start.second).timestamp())
                new_datetime_until = int(
                    datetime(1970, 1, 1, 8, minute=datetime_until.minute, second=datetime_until.second).timestamp())
                if new_datetime_start < new_datetime_until:
                    new_activities.append([new_datetime_start, new_datetime_until])
                else:
                    new_activities.append(
                        [new_datetime_start, int(datetime(1970, 1, 1, 8, minute=59, second=59).timestamp())]
                    )
                    new_activities.append(
                        [int(datetime(1970, 1, 1, 8, minute=0, second=0).timestamp()), new_datetime_until]
                    )
            new_schedule.append(tuple(new_activities))
        return tuple(new_schedule)

    def get_daily(self):
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(datetime(1970, 1, 1, hour=date.hour, minute=date.minute, second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)

    def get_weekly(self, range):
        if range:
            return tuple([316800, 921599])
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(datetime(1970, 1, date.weekday() + 5, hour=date.hour, minute=date.minute,
                                        second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)

    def get_monthly(self):
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(
                    datetime(1970, 1, date.day, hour=date.hour, minute=date.minute, second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)

    def get_annualy(self):
        new_schedule = []
        for activity in self.schedule:
            new_activity = []
            for time in activity:
                date = datetime.fromtimestamp(time)
                new_date = int(datetime(1970, date.month, date.day, hour=date.hour, minute=date.minute,
                                        second=date.second).timestamp())
                new_activity.append(new_date)
            new_schedule.append(tuple(new_activity))
        return tuple(new_schedule)
