import logging

skip_activity = []
not_merged = True


# def schedule(list_start_activities, list_end_activities):
#     """
#     convert to magic method __main__ when my_time > 0
#     """
#     if len(list_start_activities) != len(list_end_activities):
#         Exception("Jadwal tidak valid")
#         pass
#     else:
#         this_schedule = [(x, y) for x, y in list_start_activities and list_end_activities]
#         pass


def check_and_merge(activity_1, activity_2, index_of_2):
    # if activity range time reversed then reverse it again
    global not_merged
    if activity_1[0] > activity_1[1]:
        activity_1 = [activity_1[1], activity_1[0]]
    if activity_2[0] > activity_2[1]:
        activity_2 = [activity_2[1], activity_2[0]]
    not_merged = False
    if activity_1[0] > activity_2[1] or activity_2[0] > activity_1[1]:
        logging.debug(">>not merged: %s", activity_1)
        not_merged = True
        return activity_1
    elif activity_2[0] <= activity_1[0] and activity_2[1] >= activity_1[1]:
        logging.debug(">>merge a: %s", (activity_2[0], activity_2[1]))
        skip_activity.append(index_of_2)
        return activity_2[0], activity_2[1]
    elif activity_2[0] >= activity_1[0] and activity_2[1] <= activity_1[1]:
        logging.debug(">>merge b: %s", (activity_1[0], activity_1[1]))
        skip_activity.append(index_of_2)
        return activity_1[0], activity_1[1]
    elif activity_2[0] <= activity_1[0] and activity_2[1] <= activity_1[1]:
        logging.debug(">>merge c: %s", (activity_2[0], activity_1[1]))
        skip_activity.append(index_of_2)
        return activity_2[0], activity_1[1]
    elif activity_2[0] >= activity_1[0] and activity_2[1] >= activity_1[1]:
        logging.debug(">>merge d: %s", (activity_1[0], activity_2[1]))
        skip_activity.append(index_of_2)
        return activity_1[0], activity_2[1]
    else:
        # when something abnormal happens
        logging.error("!!!something wrong at line %s:\n %s\n %s ", index_of_2, activity_1, activity_2)
        pass


def merge_activities(activities):
    """
        selama masih ada range dalam schedule yang dapat digabung
        untuk setiap range dalam schedule,
        cari apakah ada range yang bisa digabung
        jika ada, gabung dan buang range tersebut.

        belum fix, algoritma masih barbar padahal activities udah di sort
    """
    activities = sorted(activities)
    logging.debug("\n")
    logging.debug("will merge: %s", activities)
    global skip_activity
    maximum_loop = len(activities) * 2
    skip_activity = []
    a = 0
    am_sure_i_can_do_something = True
    result_merged_activities = []
    while am_sure_i_can_do_something:
        for index in range(len(activities)):
            logging.debug("result_merged_activities = %s", result_merged_activities)
            try:
                if index not in skip_activity:
                    logging.debug("merging: %s and %s", activities[index], activities[index + 1])
                    temp_activity_from, temp_activity_until = check_and_merge(
                        activities[index], activities[index + 1],
                        index + 1
                    )
                    if temp_activity_until:
                        result_merged_activities.append([temp_activity_from, temp_activity_until])
                    else:
                        result_merged_activities.append(temp_activity_from)
                    logging.debug("current skip: %s", skip_activity)
            except IndexError:
                if index not in skip_activity:
                    result_merged_activities.append(activities[index])
        if activities == result_merged_activities:
            logging.debug("stop done merging")
            am_sure_i_can_do_something = False
        else:
            a += 1
            logging.debug("am_sure_i_can_do_something because:\n %s \n %s", activities, result_merged_activities)
            activities = result_merged_activities
            result_merged_activities = []
            skip_activity = []
        if a > maximum_loop:
            logging.error("error merging activities maximum loop %s:\n%s", a, activities)
            return None
    return result_merged_activities


def merge_schedules(schedules):
    if len(schedules) < 2:
        logging.debug("apparently its not many : %s \n", schedules)
        return tuple(schedules)
    result_merged_schedule = []
    for index in range(len(schedules)):
        if index == 0:
            result_merged_schedule = schedules[0]
            continue
        logging.debug("merging schedule : %s and %s\n", schedules[index], result_merged_schedule)
        new_schedule = tuple(sorted(schedules[index] + result_merged_schedule, key=lambda x: (x[0], x[1])))
        logging.debug("new_schedule : %s \n", new_schedule)
        result_merged_schedule = merge_activities(new_schedule)
    return tuple(result_merged_schedule)


def generate_merged_schedule(data):
    """
    convert to magic method __add__ when my_time > 0
    """
    schedules = []
    for schedule in data:
        activities = merge_activities(schedule)
        schedules.append(activities)
        logging.debug("merged activities : %s", activities)

    result = merge_schedules(schedules)
    logging.debug("merged schedules : %s", result)
    return result


# def check_and_divide(activity_open, activity_exist):
#     # if activity range time reversed then reverse it again
#     if activity_1[0] > activity_1[1]:
#         activity_1 = (activity_1[1], activity_1[0])
#     if activity_2[0] > activity_2[1]:
#         activity_2 = (activity_2[1], activity_2[0])
#     not_merged = False
#     if activity_1[0] > activity_2[1] or activity_2[0] > activity_1[1]:
#         logging.debug(">>not merged: %s", activity_1)
#         not_merged = True
#         return activity_1
#     elif activity_2[0] <= activity_1[0] and activity_2[1] >= activity_1[1]:
#         logging.debug(">>merge a: %s", (activity_2[0], activity_2[1]))
#         skip_activity.append(index_of_2)
#         return activity_2[0], activity_2[1]
#     elif activity_2[0] >= activity_1[0] and activity_2[1] <= activity_1[1]:
#         logging.debug(">>merge b: %s", (activity_1[0], activity_1[1]))
#         skip_activity.append(index_of_2)
#         return activity_1[0], activity_1[1]
#     elif activity_2[0] <= activity_1[0] and activity_2[1] <= activity_1[1]:
#         logging.debug(">>merge c: %s", (activity_2[0], activity_1[1]))
#         skip_activity.append(index_of_2)
#         return activity_2[0], activity_1[1]
#     elif activity_2[0] >= activity_1[0] and activity_2[1] >= activity_1[1]:
#         logging.debug(">>merge d: %s", (activity_1[0], activity_2[1]))
#         skip_activity.append(index_of_2)
#         return activity_1[0], activity_2[1]
#     else:
#         # when something abnormal happens
#         logging.error("!!!something wrong at line %s:\n %s\n %s ", index_of_2, activity_1, activity_2)
#         pass


def minus(schedule_open, schedule_exist):
    """
    convert to magic method __sub__ when my_time > 0
    """
    # equals with schedule_open - schedule_exist
    if len(schedule_exist) == 0:
        return schedule_open

    result = []
    for range_open in schedule_open:
        start = range_open[0]
        until = 0
        for range_exist in schedule_exist:
            logging.debug("\n\n")
            logging.debug("temp: %s", result)
            logging.debug("range_open: %s", range_open)
            logging.debug("range_exist: %s", range_exist)
            if range_open[1] <= range_exist[0]:
                logging.debug("range_open lower than range exist: range_open = %s and range_exist = %s",
                              range_open, range_exist)
                logging.debug("start , until = %s , %s", start, until)
                if start == range_open[0]:
                    result.append(range_open)
                break
            elif range_open[0] >= range_exist[1]:
                logging.debug("range_open higher than range exist: range_open = %s and range_exist = %s",
                              range_open, range_exist)
                logging.debug("start , until = %s , %s", start, until)
                if schedule_exist.index(range_exist) == len(schedule_exist) - 1:
                    result.append(range_open)
                continue
            elif start >= range_exist[0] and start <= range_exist[1]:
                logging.debug("start , until = %s , %s", start, until)
                start = range_exist[1]
                if range_exist[1] < range_open[1]:
                    activity = (start, range_open[1])
                    result.append(activity)
                continue
            else:
                until = range_exist[0]
                activity = (start, until)
                result.append(activity)
                start = range_exist[1]
                logging.debug("new case: %s <> %s", range_open, range_exist)
    return tuple(result)

#
# def min_time(schedules):
#     minimum = 4444444444
#     for schedule in schedules:
#         if not schedule:
#             continue
#
#         for activity in schedule:
#             for time in activity:
#                 if time < minimum:
#                     minimum = time
#     return minimum
#
#
# def max_time(schedules):
#     maximum = 0
#     for schedule in schedules:
#         if not schedule:
#             continue
#
#         for activity in schedule:
#             for time in activity:
#                 if time > maximum:
#                     maximum = time
#     return maximum
