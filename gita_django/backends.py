from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils import timezone

from phonenumber_field.validators import validate_international_phonenumber

User = get_user_model()


class EmailOrUsernameOrPhoneModelBackend:
    """
    This is a ModelBacked that allows authentication
    with either a username or an email address or phone number.
    """

    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            validate_email(username)
        except ValidationError:
            kwargs = {'username': username}
        else:
            kwargs = {'email': username}
        try:
            validate_international_phonenumber(username)
            kwargs = {'phone': username}
        except ValidationError:
            pass
        try:
            user = User.objects.get(**kwargs)
            if user and user.check_password(password):
                user.last_login = timezone.now()
                user.save()
                return user
        except User.DoesNotExist:
            pass
        return None

    def user_can_authenticate(self, user):
        """
        Reject users with is_active=False. Custom user models that don't have
        that attribute are allowed.
        """
        is_active = getattr(user, 'is_active', None)
        return is_active or is_active is None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
