from django.templatetags.static import static

from gita_emails.mails.base import Mailer
from social_network.models.verification import UserForgotPasswordToken


class AccountRetrievalMailer(Mailer):
    def __init__(self, request):
        self.request = request

    def send_recovery_url(self, user_forgot_password: UserForgotPasswordToken, recovery_url: str):
        self.template = 'account_recovery.html'
        self.subject = "Account Recovery | GiTa"
        self.email_receiver_list = [user_forgot_password.user.email]
        self.context = {
            'email': user_forgot_password.user.email,
            'logo': self.request.build_absolute_uri(static('images/logo_primary.svg')),
            'url': recovery_url,
            'full_name': user_forgot_password.user.get_full_name(),
        }
        self.send()
