from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from gita_emails.models import EmailHistory, EmailReceivers


class Mailer:
    context = {}
    subject = ""
    template = ""
    email_receiver_list = []

    # def __init__(self, **kwargs):
    #     for key in kwargs:
    #         setattr(self, key, kwargs[key])

    def send(self, attachments=None):
        if not (bool(self.subject) and bool(self.template) and bool(self.email_receiver_list)):
            raise NotImplementedError(
                'Function that you are using not correctly configured yet.\n'
                '\ncontext = {}'
                '\nsubject = {}'
                '\ntemplate = {}'
                '\nemail_receiver_list = {}'.format(
                    self.context,
                    self.subject,
                    self.template,
                    self.email_receiver_list
                )
            )
        subject = self.subject
        message_string = render_to_string(
            self.template,
            self.context
        )
        message = EmailMessage(
            subject,
            message_string,
            to=self.email_receiver_list
        )
        message.content_subtype = 'html'
        if attachments:
            for attach in attachments:
                message.attach_file(attach)
        email_history = EmailHistory.objects.create(message=message_string, status=EmailHistory.FAIL)
        for receiver in self.email_receiver_list:
            EmailReceivers.objects.create(email=email_history, receiver=receiver)
        message.send()
        email_history.status = EmailHistory.SUCCESS
        email_history.save()
