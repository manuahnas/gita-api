from django.contrib.auth import get_user_model
from django.db import models

from core_api.abstracts.base import BaseAbstractModel

User = get_user_model()


class Notification(BaseAbstractModel):
    # Relationship
    FRIEND_REQUEST = 'fr'
    FRIEND_ACCEPT = 'fa'
    # Attributes
    LIKE = 'l'
    COMMENT = 'c'
    COMMENT_ALSO = 'ca'
    # Agenda
    AGENDA_CREATED = 'gc'
    AGENDA_UPDATED = 'gu'
    # InvitationActivities
    ACTIVITY_INVITE = 'ai'
    ACTIVITY_ACCEPT = 'aa'
    ACTIVITY_REJECT = 'ar'
    # UserActivities
    ACTIVITY_CHANGED = 'ac'
    # Status
    PHOTO_TAGGED = 'pt'
    STATUS_TAGGED = 'st'

    NOTIFICATION_TYPE = (
        (FRIEND_REQUEST, '{} send you friend request'),
        (FRIEND_ACCEPT, '{} accept your friend request'),
        (LIKE, '{} like your post {}'),
        (COMMENT, '{} comment on your post {}'),
        (COMMENT_ALSO, '{} also comment on {} post'),
        (ACTIVITY_ACCEPT, '{} accept your {} invitation'),
        (ACTIVITY_INVITE, '{} invite you to join {}'),
        (ACTIVITY_CHANGED, '{} updated'),
        (PHOTO_TAGGED, 'Tag you in photo'),
        (STATUS_TAGGED, 'Tag you in post'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notifications')
    notification_type = models.CharField(max_length=2, choices=NOTIFICATION_TYPE)
    notification_related_id = models.IntegerField()
    seen = models.DateTimeField(default=None, blank=True, null=True)

    class Meta:
        ordering = ['-created_at']
