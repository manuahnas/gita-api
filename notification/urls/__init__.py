from django.urls import path

from notification.views.notification import NotificationPersonalAPIView

urlpatterns = [
    path('public/', NotificationPersonalAPIView.as_view(), name='notification-personal'),
]
