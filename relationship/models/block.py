from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models

from django.utils.translation import ugettext_lazy as _

from relationship.managers.block import BlockManager

User = get_user_model()


class Block(models.Model):
    """ Model to represent Following relationships """
    blocker = models.ForeignKey(User, related_name='blocking', on_delete=models.CASCADE)
    blocked = models.ForeignKey(User, related_name='blockees', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    objects = BlockManager()

    class Meta:
        verbose_name = _('Blocker Relationship')
        verbose_name_plural = _('Blocked Relationships')
        unique_together = ('blocker', 'blocked')

    def __str__(self):
        return "User #{} blocks #{}".format(self.blocker_id, self.blocked_id)

    def save(self, *args, **kwargs):
        # Ensure users can't be friends with themselves
        if self.blocker == self.blocked:
            raise ValidationError(_("Users cannot block themselves."))
        super().save(*args, **kwargs)
