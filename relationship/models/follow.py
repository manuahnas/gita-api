from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models

from django.utils.translation import ugettext_lazy as _

from relationship.managers.follow import FollowingManager

User = get_user_model()


class Follow(models.Model):
    """ Model to represent Following relationships """
    leader = models.ForeignKey(User, related_name='followers', on_delete=models.CASCADE)
    follower = models.ForeignKey(User, related_name='following', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    objects = FollowingManager()

    class Meta:
        verbose_name = _('Following Relationship')
        verbose_name_plural = _('Following Relationships')
        unique_together = ('follower', 'leader')

    def __str__(self):
        return "User #{} follows #{}".format(self.follower.pk, self.leader.pk)

    def save(self, *args, **kwargs):
        # Ensure users can't be friends with themselves
        if self.follower == self.leader:
            raise ValidationError(_("Users cannot follow themselves."))
        super().save(*args, **kwargs)
