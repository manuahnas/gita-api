from django.contrib.auth import get_user_model
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from core_api.caches import bust_cache
from core_api.signals import (
    friendship_request_accepted, friendship_request_rejected, friendship_request_canceled,
    friendship_request_viewed
)
from relationship.models.friendship import Friendship

User = get_user_model()


class FriendshipRequest(models.Model):
    from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friendship_requests_sent')
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friendship_requests_received')

    message = models.TextField(_('Message'), blank=True)

    created = models.DateTimeField(auto_now_add=True)
    rejected = models.DateTimeField(default=None, null=True)
    viewed = models.DateTimeField(default=None, null=True)
    retry_count = models.PositiveSmallIntegerField(default=1)

    def save(self, *args, **kwargs):
        if self._meta.model.objects.filter(from_user=self.to_user, to_user=self.from_user).exists():
            friend_request = self._meta.model.objects.get(from_user=self.to_user, to_user=self.from_user)
            friend_request.accept()
        else:
            super().save(*args, **kwargs)

    class Meta:
        verbose_name = _('Friendship Request')
        verbose_name_plural = _('Friendship Requests')
        unique_together = ('from_user', 'to_user')

    def __str__(self):
        return f"User #{self.from_user_id} friendship request to #{self.to_user_id}"

    def accept(self):
        """ Accept this friendship request """
        Friendship.objects.create(
            from_user=self.from_user,
            to_user=self.to_user
        )
        Friendship.objects.create(
            from_user=self.to_user,
            to_user=self.from_user
        )

        friendship_request_accepted.send(
            sender=self,
            from_user=self.from_user,
            to_user=self.to_user
        )

        self.delete()

        # Delete any reverse requests
        FriendshipRequest.objects.filter(
            from_user=self.to_user,
            to_user=self.from_user
        ).delete()

        # Bust requests cache - request is deleted
        bust_cache('requests', self.to_user.pk)
        bust_cache('sent_requests', self.from_user.pk)
        # Bust reverse requests cache - reverse request might be deleted
        bust_cache('requests', self.from_user.pk)
        bust_cache('sent_requests', self.to_user.pk)
        # Bust friends cache - new friends added
        bust_cache('friends', self.to_user.pk)
        bust_cache('friends', self.from_user.pk)

        return True

    def reject(self):
        """ reject this friendship request """
        self.rejected = now()
        self.save()
        friendship_request_rejected.send(sender=self)
        bust_cache('requests', self.to_user.pk)

    def cancel(self):
        """ cancel this friendship request """
        self.delete()
        friendship_request_canceled.send(sender=self)
        bust_cache('requests', self.to_user.pk)
        bust_cache('sent_requests', self.from_user.pk)
        return True

    def mark_viewed(self):
        self.viewed = now()
        friendship_request_viewed.send(sender=self)
        self.save()
        bust_cache('requests', self.to_user.pk)
        return True
