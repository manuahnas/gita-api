from django.urls import path, include

urlpatterns = [
    path('friendship/', include('relationship.urls.friendship'), name='friendship'),
    # path('follow/', include('relationship.urls.follow'), name='follow'),
    # path('block/', include('relationship.urls.block'), name='blocks'),
    # path('settings/', include('relationship.urls.settings'), name='settings'),
]
