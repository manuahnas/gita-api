from django.urls import path

from relationship.views.friendship import (
    FriendListAPIView, FriendRequestListAPIView, FriendRequestCreateAPIView, FriendRequestCancelAPIView,
    FriendRequestAcceptAPIView, FriendRequestRejectAPIView, FriendRemoveAPIView,
    OtherFriendListAPIView)

urlpatterns = [
    path('', FriendListAPIView.as_view(), name='friendship-personal'),
    path('other/<str:username>/', OtherFriendListAPIView.as_view(), name='friendship-personal'),
    path('friend-requests/', FriendRequestListAPIView.as_view(), name='friendship-requests'),
    path('add/', FriendRequestCreateAPIView.as_view(), name='friendship-add'),
    path('cancel-request/', FriendRequestCancelAPIView.as_view(), name='friendship-cancel_request'),
    path('accept-request/', FriendRequestAcceptAPIView.as_view(), name='friendship-accept_request'),
    path('reject-request/', FriendRequestRejectAPIView.as_view(), name='friendship-reject_request'),
    path('remove/', FriendRemoveAPIView.as_view(), name='friendship-remove'),
]
