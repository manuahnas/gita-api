from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from core_api.exceptions import AlreadyExistsError
from core_api.permissions import IsDefaultAuthenticatedUser
from relationship.models import Friendship, FriendshipRequest
from relationship.serializers.friendship import (
    FriendSerializer, FriendRequestSerializer, FriendRequestActionSerializer,
)

User = get_user_model()


class FriendListAPIView(generics.ListAPIView):
    serializer_class = FriendSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)
    search_fields = ('from_user__username', 'from_user__first_name', 'from_user__last_name',)

    def get_queryset(self):
        return Friendship.objects.get_friends(self.request.user)


class OtherFriendListAPIView(generics.ListAPIView):
    serializer_class = FriendSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        user = User.objects.get(username=self.kwargs['username'])
        return Friendship.objects.get_friends(user)


class FriendRequestListAPIView(generics.ListAPIView):
    serializer_class = FriendRequestSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def get_queryset(self):
        return Friendship.objects.get_requests(self.request.user)


class FriendRequestCreateAPIView(generics.CreateAPIView):
    serializer_class = FriendRequestActionSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        headers = self.get_success_headers(serializer.data)
        username = serializer.validated_data['username']
        if User.objects.filter(username=username).exists():
            to_user = User.objects.get(username=username)
            try:
                Friendship.objects.add_friend(request.user, to_user)
            except AlreadyExistsError as e:
                return Response({'errors': str(e)}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            except ValidationError as e:
                return Response({'errors': e.message}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            else:
                return Response(
                    {'messages': _('Friend request sent.')},
                    status=status.HTTP_201_CREATED,
                    headers=headers
                )
        error_msg = _('User with username {} does not exist.')
        return Response(
            {'errors': error_msg.format(username)},
            status=status.HTTP_422_UNPROCESSABLE_ENTITY
        )


class FriendRequestCancelAPIView(generics.DestroyAPIView):
    serializer_class = FriendRequestActionSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def destroy(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data['username']
        if User.objects.filter(username=username).exists():
            to_user = User.objects.get(username=username)
            if FriendshipRequest.objects.filter(from_user=self.request.user, to_user=to_user).exists():
                instance = FriendshipRequest.objects.get(from_user=self.request.user, to_user=to_user)
                self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            error_msg = _('User with username {} does not exist.')
            return Response(
                {'errors': error_msg.format(username)},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )

    def perform_destroy(self, instance: FriendshipRequest):
        instance.cancel()


class FriendRequestAcceptAPIView(generics.CreateAPIView):
    serializer_class = FriendRequestActionSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        headers = self.get_success_headers(serializer.data)
        username = serializer.validated_data['username']
        if User.objects.filter(username=username).exists():
            from_user = User.objects.get(username=username)
            if FriendshipRequest.objects.filter(from_user=from_user, to_user=self.request.user).exists():
                instance = FriendshipRequest.objects.get(from_user=from_user, to_user=self.request.user)
                instance.accept()
                return Response(status=status.HTTP_200_OK, headers=headers)
            else:
                return Response(
                    {'errors': _('Friend request not found.')},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )
        else:
            error_msg = _('User with username {} does not exist.')
            return Response(
                {'errors': error_msg.format(username)},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


class FriendRequestRejectAPIView(generics.CreateAPIView):
    serializer_class = FriendRequestActionSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        headers = self.get_success_headers(serializer.data)
        username = serializer.validated_data['username']
        if User.objects.filter(username=username).exists():
            from_user = User.objects.get(username=username)
            if FriendshipRequest.objects.filter(from_user=from_user, to_user=self.request.user).exists():
                instance = FriendshipRequest.objects.get(from_user=from_user, to_user=self.request.user)
                instance.reject()
                return Response(status=status.HTTP_200_OK, headers=headers)
            else:
                return Response(
                    {'errors': _('Friend request not found.')},
                    status=status.HTTP_422_UNPROCESSABLE_ENTITY
                )
        else:
            error_msg = _('User with username {} does not exist.')
            return Response(
                {'errors': error_msg.format(username)},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


class FriendRemoveAPIView(generics.DestroyAPIView):
    serializer_class = FriendRequestActionSerializer
    permission_classes = (IsDefaultAuthenticatedUser,)

    def destroy(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data['username']
        if User.objects.filter(username=username).exists():
            from_user = User.objects.get(username=username)
            Friendship.objects.remove_friend(self.request.user, from_user)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            error_msg = _('User with username {} does not exist.')
            return Response(
                {'errors': error_msg.format(username)},
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )
