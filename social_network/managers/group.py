from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.db import models

from core_api.caches import cache_key

User = get_user_model()


class GroupProfileManager(models.Manager):
    """ GroupProfile manager """

    def joined_groups(self, user):
        """ Returns queryset of all groups joined by the user """
        key = cache_key('groups', user.pk)
        groups = cache.get(key)
        if groups is None:
            from social_network.models import GroupMembers, GroupProfile
            groups = GroupProfile.objects.filter(members__member=user).order_by('-id')
            cache.set(key, groups)
        return groups

    def administered_groups(self, user: User):
        """ Returns queryset of all groups being administered by the user """
        key = cache_key('groups', user.pk)
        locations = cache.get(key)
        if locations is None:
            locations = self.objects.filter(admins__admin=user)
            cache.set(key, locations)
        return locations


class GroupMembersManager(models.Manager):
    """ GroupMembers manager """

    def user_groups(self, user):
        """ Return a list of all user groups """
        key = cache_key('groups', user.pk)
        groups = cache.get(key)
        if groups is None:
            from social_network.models import GroupMembers
            groups = GroupMembers.objects.select_related('group').filter(member=user).all().order_by('-id')
            cache.set(key, groups)
        return groups
