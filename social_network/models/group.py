from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.db import models
from django.utils.timezone import now
from sorl.thumbnail import ImageField as ThumbnailImageField

from core_api.abstracts.username import UsernameAbstractModel
from core_api.caches import cache_key
from social_network.managers.group import GroupProfileManager, GroupMembersManager

User = get_user_model()


def group_avatar_directory_path(instance, filename):
    return 'group_{0}/{1}'.format(instance.id, filename)


def group_cover_directory_path(instance, filename):
    return 'group_{0}/{1}'.format(instance.id, filename)


class GroupProfile(UsernameAbstractModel):
    group_name = models.CharField(max_length=55)
    about = models.TextField(max_length=255, default='', blank=True)
    profile_picture = ThumbnailImageField(
        upload_to=group_avatar_directory_path,
        default='images/default_group_avatar.jpeg'
    )
    profile_cover = models.ImageField(
        upload_to=group_cover_directory_path,
        default='images/default_group_cover.jpeg'
    )
    creator = models.ForeignKey(User, models.SET_NULL, related_name='created_groups', null=True)
    objects = GroupProfileManager()

    def __str__(self):
        return self.group_name

    def save(self, *args, **kwargs):
        # TODO: seems like using signal would be better
        is_new = not self.pk
        super().save(*args, **kwargs)
        if is_new:
            GroupMembers.objects.create(group=self, member=self.creator, admin=now())
            key = cache_key('groups', self.creator.pk)
            cache.delete(key)

    def add_member(self, user: User):
        GroupMembers.objects.create(group=self, member=user)

    def is_member(self, user: User):
        return GroupMembers.objects.filter(group=self, member=user).exists()

    def is_admin(self, user: User):
        return GroupMembers.objects.filter(group=self, member=user, admin__isnull=False).exists()

    def remove_member(self, member: User):
        GroupMembers.objects.get(group=self, member=member).delete()

    def add_admin(self, user: User):
        member, created = GroupMembers.objects.get_or_create(group=self, member=user)
        if not member.admin:
            member.admin = now()
            member.save()

    def remove_admin(self, member: User):
        if GroupMembers.objects.filter(group=self, member=member).exists():
            member = GroupMembers.objects.get(group=self, member=member)
            member.admin = None
            member.save()


class GroupMembers(models.Model):
    group = models.ForeignKey(GroupProfile, on_delete=models.CASCADE, related_name='members')
    member = models.ForeignKey(User, models.CASCADE, related_name='member_of_groups')
    join_date = models.DateTimeField(auto_now=True)
    admin = models.DateTimeField(null=True, blank=True)
    objects = GroupMembersManager()

    def __str__(self):
        return '{} {} group of {}'.format(
            self.member.get_full_name(),
            'admin' if self.admin else 'member',
            self.group.group_name
        )

    class Meta:
        unique_together = ('group', 'member')
