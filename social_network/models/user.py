from django.contrib.auth.models import AbstractUser, UserManager
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from phonenumbers import PhoneNumber

from core_api.abstracts.base import BaseAbstractModel


class GitaUserManager(UserManager):
    def _create_user(self, phone, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not phone:
            raise ValueError('The given phone must be set')
        email = self.normalize_email(email)
        extra_fields['phone'] = phone
        extra_fields['email'] = email

        if not extra_fields.get('username', None):
            extra_fields['username'] = str(phone)
        user_with_username = self.model.objects.filter(username=extra_fields['username'])
        if user_with_username.filter(phone_verified__isnull=False).exists():
            raise ValidationError('User with this username already registered.')
        elif user_with_username.exists():
            user_with_username.delete()
        user = self.model(**extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone, password=None, email=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone, email, password, **extra_fields)

    def create_gita_user(self, phone: PhoneNumber, data_profile: dict, password=None, email=None, **extra_fields):
        from social_network.models import UserProfile
        extra_fields['is_staff'] = False
        extra_fields['is_superuser'] = False
        user = self._create_user(phone, email, password, **extra_fields)
        UserProfile.objects.create(user=user, **data_profile)
        return user

    def create_superuser(self, phone, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, email, password, **extra_fields)


class GitaUser(AbstractUser):
    phone = PhoneNumberField(
        _('phone number'), unique=True, blank=False, null=False,
        error_messages={
            'unique': _("This phone number already used."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=30)
    phone_verified = models.DateTimeField(default=None, null=True)
    email_verified = models.DateTimeField(default=None, null=True)

    objects = GitaUserManager()

    class Meta:
        ordering = ('username',)

    def __str__(self):
        return self.username


class UserPhones(BaseAbstractModel):
    user = models.ForeignKey(GitaUser, on_delete=models.CASCADE, related_name='phones')
    phone = PhoneNumberField(
        _('phone number'), unique=True,
        error_messages={
            'unique': _("This phone number already used."),
        },
    )
    phone_verified = models.DateTimeField(blank=True, null=True)


class UserEmails(BaseAbstractModel):
    user = models.ForeignKey(GitaUser, on_delete=models.CASCADE, related_name='emails')
    email = models.EmailField()
    email_verified = models.DateTimeField(blank=True, null=True)
    is_primary = models.BooleanField(blank=True)

    def save(self, *args, **kwargs):
        user_emails = UserEmails.objects.filter(user=self.user)
        if self.is_primary and user_emails.exists():
            user_emails.update(is_primary=False)
            self.user.email = self.email
            self.user.save()
        elif self.is_primary and not user_emails.exists():
            self.is_primary = True
            self.user.email = self.email
            self.user.save()
        else:
            self.is_primary = True
            self.user.email = self.email
            self.user.save()
        super().save(*args, **kwargs)
