from django.contrib.auth import get_user_model
from django.db import models

from core_api.abstracts.comment import AbstractComments
from core_api.abstracts.like import AbstractLikes
from core_api.abstracts.wall_post import AbstractWallPost
from social_network.models import LocationProfile, GroupProfile

User = get_user_model()


class WallPost(AbstractWallPost):
    wall_of_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_wall')


class WallPostLikes(AbstractLikes):
    wall_post = models.ForeignKey(WallPost, on_delete=models.CASCADE, related_name='likes')


class WallPostComment(AbstractComments):
    wall_post = models.ForeignKey(WallPost, on_delete=models.CASCADE, related_name='comments')


class GroupWallPost(AbstractWallPost):
    wall_of_group = models.ForeignKey(GroupProfile, on_delete=models.CASCADE, related_name='group_wall')


class GroupWallPostLikes(AbstractLikes):
    wall_post = models.ForeignKey(GroupWallPost, on_delete=models.CASCADE, related_name='likes')


class GroupWallPostComment(AbstractComments):
    wall_post = models.ForeignKey(GroupWallPost, on_delete=models.CASCADE, related_name='comments')


class LocationWallPost(AbstractWallPost):
    wall_of_location = models.ForeignKey(LocationProfile, on_delete=models.CASCADE, related_name='location_wall')


class LocationWallPostLikes(AbstractLikes):
    wall_post = models.ForeignKey(LocationWallPost, on_delete=models.CASCADE, related_name='likes')


class LocationWallPostComment(AbstractComments):
    wall_post = models.ForeignKey(LocationWallPost, on_delete=models.CASCADE, related_name='comments')
