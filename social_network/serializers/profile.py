from django.contrib.auth import get_user_model
from rest_framework import serializers

from relationship.models import Friendship
from social_network.models import GroupMembers, LocationAdmins
from social_network.models.profile import UserProfile

User = get_user_model()


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'


class UserGroupMembersSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupMembers
        depth = 1
        exclude = ('member',)


class UserLocationAdminsSerializer(serializers.ModelSerializer):
    class Meta:
        model = LocationAdmins
        depth = 1
        exclude = ('admin',)


class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(many=False)
    full_name = serializers.SerializerMethodField()
    administrated_locations = UserLocationAdminsSerializer(many=True, read_only=True)
    member_of_groups = UserGroupMembersSerializer(many=True, read_only=True)

    @staticmethod
    def get_full_name(obj):
        return obj.get_full_name()

    class Meta:
        model = User
        read_only_fields = ('date_joined',)
        exclude = ('user_permissions', 'groups', 'is_staff', 'is_superuser',)
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile')
        instance.username = profile_data.get('username', instance.username)
        instance.first_name = profile_data.get('first_name', instance.first_name)
        instance.last_name = profile_data.get('last_name', instance.last_name)
        instance.email = profile_data.get('email', instance.email)
        instance.save()
        instance.profile.about = profile_data.get('about', instance.profile.about)
        instance.profile.birthday = profile_data.get('birthday', instance.profile.birthday)
        instance.profile.profile_picture = profile_data.get('profile_picture', instance.profile.profile_picture)
        instance.profile.profile_cover = profile_data.get('profile_cover', instance.profile.profile_cover)
        instance.profile.gender = profile_data.get('gender', instance.profile.gender)
        instance.save()
        return instance


class UserProfileAvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('avatar',)


class UserProfileCoverSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('cover',)


class OtherUserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('about', 'avatar', 'cover', 'gender', 'birthday',)


class OtherUserSerializer(serializers.ModelSerializer):
    profile = OtherUserProfileSerializer(many=False)
    full_name = serializers.SerializerMethodField()
    can_request_send = serializers.SerializerMethodField()
    are_friends = serializers.SerializerMethodField()

    @staticmethod
    def get_full_name(obj: User):
        return obj.get_full_name()

    def get_can_request_send(self, obj: User):
        return Friendship.objects.can_request_send(self.context['request'].user, obj)

    def get_are_friends(self, obj: User):
        return Friendship.objects.are_friends(self.context['request'].user, obj)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'full_name', 'date_joined', 'profile',
            'can_request_send', 'are_friends',
        )
