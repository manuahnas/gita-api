from django.contrib.humanize.templatetags.humanize import naturaltime
from rest_framework import serializers

from social_network.models.walls import WallPost, GroupWallPost, LocationWallPost
from social_network.serializers.group import GroupSerializer
from social_network.serializers.location import LocationSerializer
from social_network.serializers.profile import OtherUserSerializer


class WallSerializer(serializers.ModelSerializer):
    wall_of_user = OtherUserSerializer(many=False, read_only=True)
    user = OtherUserSerializer(many=False, read_only=True)
    created_at_natural = serializers.SerializerMethodField()
    updated_at_natural = serializers.SerializerMethodField()

    @staticmethod
    def get_created_at_natural(obj: WallPost):
        return naturaltime(obj.created_at)

    @staticmethod
    def get_updated_at_natural(obj: WallPost):
        return naturaltime(obj.updated_at)

    class Meta:
        model = WallPost
        fields = (
            'id', 'body', 'created_at', 'created_at_natural', 'updated_at', 'updated_at_natural', 'wall_of_user',
            'user',
        )


class GroupWallSerializer(serializers.ModelSerializer):
    wall_of_group = GroupSerializer(many=False, read_only=True)
    user = OtherUserSerializer(many=False, read_only=True)

    class Meta:
        model = GroupWallPost
        fields = ('id', 'body', 'created_at', 'updated_at', 'wall_of_group', 'user',)


class LocationWallSerializer(serializers.ModelSerializer):
    wall_of_location = LocationSerializer(many=False, read_only=True)
    user = OtherUserSerializer(many=False, read_only=True)

    class Meta:
        model = LocationWallPost
        fields = ('id', 'body', 'created_at', 'updated_at', 'wall_of_location', 'user',)
