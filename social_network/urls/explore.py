from django.urls import path

from social_network.views.explore import ExploreAPIView

urlpatterns = [
    path('', ExploreAPIView.as_view(), name='explore'),
]
