from django.urls import path

from social_network.views.feed import FeedListAPIView

urlpatterns = [
    path('', FeedListAPIView.as_view(), name='feed'),
]
