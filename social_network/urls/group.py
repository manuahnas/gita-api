from django.urls import path

from social_network.views.group import (
    GroupAPIView, GroupCreateAPIView, UserGroupPersonalListAPIView, GroupManageAPIView,
    GroupDetailAPIView, GroupMemberAPIView, GroupMemberAddAPIView, GroupMemberRemoveAPIView, GroupAdminPromoteAPIView,
    GroupAdminDemoteAPIView
)

urlpatterns = [
    path('', GroupAPIView.as_view(), name='group_search'),
    path('create/', GroupCreateAPIView.as_view(), name='group_create'),
    path('personal/', UserGroupPersonalListAPIView.as_view(), name='group_personal'),
    path('manage/<int:group_id>/', GroupManageAPIView.as_view(), name='group_manage'),
    path('detail/<int:group_id>/', GroupDetailAPIView.as_view(), name='group_detail'),
    path('members/<int:group_id>/', GroupMemberAPIView.as_view(), name='group_members'),
    path('add-member/<int:group_id>/', GroupMemberAddAPIView.as_view(), name='group_add-member'),
    path('remove-member/<int:group_id>/', GroupMemberRemoveAPIView.as_view(), name='group_remove-member'),
    path('promote-admin/<int:group_id>/', GroupAdminPromoteAPIView.as_view(), name='group_promote-admin'),
    path('demote-admin/<int:group_id>/', GroupAdminDemoteAPIView.as_view(), name='group_demote-admin'),
]
