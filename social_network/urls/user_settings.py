from django.urls import path

from social_network.views.user_settings import (
    GeneralSettingAPIView, SecuritySettingAPIView
)

urlpatterns = [
    path('', GeneralSettingAPIView.as_view(), name='settings_general'),
    path('security/', SecuritySettingAPIView.as_view(), name='settings_security'),
]
