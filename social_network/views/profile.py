from django.contrib.auth import get_user_model
from rest_framework import generics

from core_api import permissions
from social_network.models.profile import UserProfile
from social_network.serializers.profile import (
    UserProfileAvatarSerializer, UserProfileCoverSerializer, OtherUserSerializer,
    UserSerializer)

User = get_user_model()


class UserProfileAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    permission_classes = (permissions.IsUserOwnerAndAuthenticatedUser,)

    def get_object(self):
        return self.request.user


class UserProfileAvatarAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = UserProfileAvatarSerializer
    permission_classes = (permissions.IsProfileOwnerAndAuthenticatedUser,)

    def get_object(self):
        return UserProfile.objects.get(user=self.request.user)


class UserProfileCoverAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = UserProfileCoverSerializer
    permission_classes = (permissions.IsProfileOwnerAndAuthenticatedUser,)

    def get_object(self):
        return UserProfile.objects.get(user=self.request.user)


class OtherUserProfileAPIView(generics.RetrieveAPIView):
    serializer_class = OtherUserSerializer
    permission_classes = (permissions.IsPublicAndAuthenticatedUser,)
    lookup_field = 'username'
    lookup_url_kwarg = 'username'

    def get_object(self):
        return User.objects.get(username=self.kwargs.get('username'))
