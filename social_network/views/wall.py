from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics, exceptions

from social_network.models import (
    GroupProfile, LocationProfile, WallPost, GroupWallPost, LocationWallPost
)
from core_api.permissions.wall import (
    IsMemberOfGroupAndAuthenticatedUser, IsFriendOfUserAndAuthenticatedUser, IsAdminOfLocationAndAuthenticatedUser
)
from social_network.serializers.wall import (
    WallSerializer, GroupWallSerializer, LocationWallSerializer,
)

User = get_user_model()


class UserWallAPIView(generics.ListCreateAPIView):
    serializer_class = WallSerializer
    permission_classes = (IsFriendOfUserAndAuthenticatedUser,)

    def get_queryset(self):
        wall_of_user = self.get_object()
        return WallPost.objects.filter(wall_of_user=wall_of_user)

    def perform_create(self, serializer):
        serializer.save(
            wall_of_user=self.get_object(),
            user=self.request.user
        )

    def get_object(self):
        if self.kwargs['user_id'] == 0:
            return self.request.user
        if not User.objects.filter(id=self.kwargs['user_id']).exists():
            raise exceptions.ValidationError(
                _('User with this id does not exists.'), 'create_read_wall_post_user_invalid_user_id'
            )
        obj = User.objects.get(id=self.kwargs['user_id'])
        self.check_object_permissions(self.request, obj)
        return obj


class GroupWallAPIView(generics.ListCreateAPIView):
    serializer_class = GroupWallSerializer
    permission_classes = (IsMemberOfGroupAndAuthenticatedUser,)

    def get_queryset(self):
        wall_of_group = self.get_object()
        return GroupWallPost.objects.filter(wall_of_group=wall_of_group)

    def perform_create(self, serializer):
        serializer.save(
            wall_of_group=self.get_object(),
            user=self.request.user
        )

    def get_object(self):
        if not GroupProfile.objects.filter(id=self.kwargs['group_id']).exists():
            raise exceptions.ValidationError(
                _('Group with this id does not exists.'), 'create_read_wall_post_group_invalid_group_id'
            )
        obj = GroupProfile.objects.get(id=self.kwargs['group_id'])
        self.check_object_permissions(self.request, obj)
        return obj


class LocationWallAPIView(generics.ListCreateAPIView):
    serializer_class = LocationWallSerializer
    permission_classes = (IsAdminOfLocationAndAuthenticatedUser,)

    def get_queryset(self):
        wall_of_location = self.get_object()
        return LocationWallPost.objects.filter(wall_of_location=wall_of_location)

    def perform_create(self, serializer):
        serializer.save(
            wall_of_location=self.get_object(),
            user=self.request.user
        )

    def get_object(self):
        if not LocationProfile.objects.filter(id=self.kwargs['location_id']).exists():
            raise exceptions.ValidationError(
                _('Location with this id does not exists.'), 'create_read_wall_post_location_invalid_location_id'
            )
        obj = LocationProfile.objects.get(id=self.kwargs['location_id'])
        self.check_object_permissions(self.request, obj)
        return obj
