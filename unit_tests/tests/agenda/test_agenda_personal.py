from datetime import timedelta

import pytz
from django.conf import settings
from django.urls import reverse
from rest_framework import status

from agenda.models import Agenda
from unit_tests.tests.common import PreparedTestCase


class AgendaPersonalTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()

    def get_personal_agendas(self, user_index: int):
        response = self.client_users[user_index].get(reverse('agenda-personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        return response.data

    def test_create_personal_once_agenda(self):
        user_index = 4
        personal_once_agenda_data = {
            'agenda_name': 'User Individual One-Time Agenda',
            'description': 'User personal agenda that will '
                           'only generated once or not repeat in specific range time',
            'loop': Agenda.NO_LOOP,
            'range_time_start': '{}-{}-{}T8:00:00+07:00'.format(
                self.tomorrow.year, str(self.tomorrow.month).zfill(2), str(self.tomorrow.day).zfill(2)
            ),
            'range_time_until': '{}-{}-{}T15:00:00+07:00'.format(
                self.tomorrow.year, str(self.tomorrow.month).zfill(2), str(self.tomorrow.day).zfill(2)
            ),
            'locations': (
                {'location': self.object_locations[0].id},
                {'location': self.object_locations[1].id},
            ),
            'activities': (
                {
                    'activity_name': 'Berenang',
                    'description': 'Segerin diri, berenang dulu',
                    'duration': 3600,  # one hour
                },
            ),
        }
        response = self.client_users[user_index].post(reverse('agenda-personal'), personal_once_agenda_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        agenda = Agenda.objects.get(id=response.data['id'])
        agenda.generate_activity()
        for user_activity in self.object_users[user_index].activities.all():
            print(
                user_activity.activity.activity_name,
                user_activity.activity.time_start.astimezone(pytz.timezone(settings.TIME_ZONE)),
                user_activity.activity.time_until.astimezone(pytz.timezone(settings.TIME_ZONE))
            )

        return user_index, response.data

    def test_create_personal_repeat_agenda(self):
        user_index = 4
        next_month = self.tomorrow + timedelta(days=30)
        personal_repeat_agenda_data = {
            'agenda_name': 'User Individual Repeat Agenda',
            'description': 'User personal agenda that will '
                           'generated multiple times daily in multiple time range for each day',
            'loop': Agenda.DAILY,
            'loop_interlude': 1,  # equals with everyday
            # 'loop_count': 30,  # equals with repeat for a month
            'range_time_start': '{}-{}-{}T00:00:00+07:00'.format(self.tomorrow.year,
                                                                 str(self.tomorrow.month).zfill(2),
                                                                 str(self.tomorrow.day).zfill(2)),
            'range_time_until': '{}-{}-{}T00:00:00+07:00'.format(next_month.year,
                                                                 str(next_month.month).zfill(2),
                                                                 str(next_month.day).zfill(2)),
            'locations': (
                {'location': self.object_locations[0].id},
                {'location': self.object_locations[1].id},
            ),
            'activities': (
                {
                    'activity_name': 'Mandi',
                    'description': 'Bersihin diri, mandi dulu',
                    'duration': 600,  # 10 minutes
                },
            ),
        }
        response = self.client_users[user_index].post(reverse('agenda-personal'), personal_repeat_agenda_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_create_joint_once_agenda(self):
        user_index = 4
        joint_once_agenda = {
            'agenda_name': 'Users (Party) One-Time Agenda',
            'description': 'User party (with it friend/s by tagging them) agenda '
                           'that will only generated once or not repeat in specific range time',
            'loop': Agenda.NO_LOOP,
            'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(self.tomorrow.year,
                                                                 str(self.tomorrow.month).zfill(2),
                                                                 str(self.tomorrow.day).zfill(2)),
            'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
                self.tomorrow.year, str(self.tomorrow.month).zfill(2), str(self.tomorrow.day).zfill(2)
            ),
            'locations': (
                {'location': self.object_locations[0].id},
                {'location': self.object_locations[1].id},
            ),
            'activities': (
                {
                    'activity_name': 'Lari Pagi',
                    'description': 'Segerin badan, lari pagi bareng dulu',
                    'duration': 3600,
                    'hosts': (
                        {'host': self.object_users[0].id},
                    ),
                    'members': (
                        {'member': self.object_users[1].id},
                        {'member': self.object_users[2].id},
                        {'member': self.object_users[3].id},
                        {'member': self.object_users[4].id},
                        {'member': self.object_users[5].id},
                        {'member': self.object_users[6].id},
                        {'member': self.object_users[7].id},
                    )
                },
            ),
        }
        response = self.client_users[user_index].post(reverse('agenda-personal'), joint_once_agenda)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_create_joint_repeat_agenda(self):
        user_index = 4
        joint_repeat_agenda = {
            'agenda_name': 'Users (Party) Repeat Agenda',
            'description': 'User party (with it friend/s by tagging them) agenda '
                           'that will generated multiple times in multiple time range',
            'loop': Agenda.WEEKLY,
            'loop_interlude': 2,  # equals with every two week
            # 'loop_count': 4,  # equals with in between 90 days, do it 4 times
            'range_time_start': '{}-{}-{}T03:30:00+07:00'.format(self.tomorrow.year,
                                                                 str(self.tomorrow.month).zfill(2),
                                                                 str(self.tomorrow.day).zfill(2)),
            'range_time_until': '{}-{}-{}T09:00:00+07:00'.format(
                self.tomorrow.year, str(self.tomorrow.month).zfill(2), (self.tomorrow + timedelta(days=90)).day
            ),
            'locations': (
                {'location': self.object_locations[0].id},
                {'location': self.object_locations[1].id},
            ),
            'activities': (
                {
                    'activity_name': 'Badminton',
                    'description': 'Latih respon, badminton bareng dulu',
                    'duration': 3600,
                    'hosts': (
                        {'host': self.object_users[0].id},
                    ),
                    'members': (
                        {'member': self.object_users[1].id},
                        {'member': self.object_users[2].id},
                        {'member': self.object_users[3].id},
                        {'member': self.object_users[4].id},
                        {'member': self.object_users[5].id},
                        {'member': self.object_users[6].id},
                        {'member': self.object_users[7].id},
                    )
                },
            ),
        }
        response = self.client_users[user_index].post(reverse('agenda-personal'), joint_repeat_agenda)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

    def test_read_personal_agendas(self):
        user_index = 4
        response = self.client_users[user_index].get(reverse('agenda-personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_update_personal_agenda(self):
        user_index, agenda_data = self.test_create_personal_once_agenda()
        agenda_update_data = {
            'agenda_name': 'User Individual One-Time Agenda Updated',
            'description': 'User personal agenda that will '
                           'only generated once or not repeat in specific range time updated',
            'loop': Agenda.NO_LOOP,
            'range_time_start': '{}-{}-{}T1:00:00+07:00'.format(
                self.tomorrow.year, str(self.tomorrow.month).zfill(2), str(self.tomorrow.day).zfill(2)
            ),
            'range_time_until': '{}-{}-{}T19:00:00+07:00'.format(
                self.tomorrow.year, str(self.tomorrow.month).zfill(2), str(self.tomorrow.day).zfill(2)
            ),
            'locations': [
                {'location': self.object_locations[0].id},
            ],
            'activities': [
                {
                    'activity_name': 'Berenang updated',
                    'description': 'Segerin diri, berenang dulu updated',
                    'duration': 1800,
                },
            ],
        }
        response = self.client_users[user_index].patch(
            reverse('agenda-detail_personal', kwargs={'agenda_id': agenda_data['id']}),
            agenda_update_data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            response.data['agenda_name'],
            agenda_update_data['agenda_name']
        )
        self.assertEqual(
            response.data['activities'][0]['activity_name'],
            agenda_update_data['activities'][0]['activity_name']
        )

    def test_delete_personal_agenda(self):
        user_index, agenda_data = self.test_create_personal_once_agenda()
        response = self.client_users[user_index].delete(
            reverse('agenda-detail_personal', kwargs={'agenda_id': agenda_data['id']})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)
        self.assertEqual(
            Agenda.objects.filter(
                user=self.object_users[user_index],
                agenda_name=agenda_data['agenda_name']
            ).count(),
            0
        )
        response_data = self.get_personal_agendas(user_index)
        self.assertEqual(response_data['count'], 0)
