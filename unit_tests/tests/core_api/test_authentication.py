from datetime import timedelta
from random import randint

from django.contrib.auth import get_user_model
from django.utils.timezone import now
from rest_framework import status
from rest_framework.settings import api_settings

from unit_tests.tests.common import PreparedTestCase

User = get_user_model()


class AuthenticationTest(PreparedTestCase):
    def test_check_username_valid(self):
        check_username_data = {
            'username': 'virtualboyfriend',
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_check_username'], check_username_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        """
        response.data= {
         'is_valid' : true
        }
        """
        self.assertTrue(response.data['is_valid'], response.data)

    def test_check_username_invalid(self):
        check_username_data = {
            'username': 'dimasinchidi',
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_check_username'], check_username_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertFalse(response.data['is_valid'], response.data)

    def test_register_success(self):
        register_data = {
            'phone': '+628152121215',
            'first_name': 'Virtual',
            'last_name': 'Boyfriend',
            'username': 'virtualboyfriend',
            'password': 's7r0n9p455',
            'birthday': '{:04d}-{:02d}-{:02d}'.format(randint(1950, 2010), randint(1, 12), randint(1, 28)),
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_register_user'], register_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        # response should not send back password
        self.assertRaises(KeyError, lambda: response.data['password'])
        for key, value in response.data.items():
            self.assertEqual(value, register_data[key])
        return register_data

    def test_register_fail_phone_registered(self):
        register_data = {
            'phone': '+628152121216',
            'first_name': 'Virtual',
            'last_name': 'Boyfriend',
            'username': 'virtualboyfriend',
            'password': 's7r0n9p455',
            'birthday': '{:04d}-{:02d}-{:02d}'.format(randint(1950, 2010), randint(1, 12), randint(1, 28)),
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_register_user'], register_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        # response should send back phone error message
        assert response.data['phone']

    def test_register_fail_username_registered(self):
        register_data = {
            'phone': '+628152121215',
            'first_name': 'Virtual',
            'last_name': 'Boyfriend',
            'username': 'dimasinchidi',
            'password': 's7r0n9p455',
            'birthday': '{:04d}-{:02d}-{:02d}'.format(randint(1950, 2010), randint(1, 12), randint(1, 28)),
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_register_user'], register_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        # response should send back username error message
        assert response.data['username']

    def test_register_fail_phone_and_username_registered(self):
        register_data = {
            'phone': '+628152121216',
            'first_name': 'Virtual',
            'last_name': 'Boyfriend',
            'username': 'dimasinchidi',
            'password': 's7r0n9p455',
            'birthday': '{:04d}-{:02d}-{:02d}'.format(randint(1950, 2010), randint(1, 12), randint(1, 28)),
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_register_user'], register_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        # response should send back phone and username error message
        assert response.data['phone']

    def test_resend_token_success(self):
        from social_network.models.verification import UserPhoneVerificationToken
        register_data = self.test_register_success()
        old_verification = UserPhoneVerificationToken.objects.get(user__phone=register_data['phone'])
        old_verification.created_at = now() - timedelta(minutes=3)
        old_verification.save()
        resend_token_data = {
            'phone': register_data['phone'],
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_verify_token_resend'], resend_token_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        new_verification = UserPhoneVerificationToken.objects.get(user__phone=register_data['phone'])
        self.assertNotEqual(old_verification.token, new_verification.token)
        self.assertNotEqual(old_verification.created_at, new_verification.created_at)

    def test_resend_token_phone_already_verified(self):
        resend_token_data = {
            'phone': str(self.object_users[0].phone),
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_verify_token_resend'], resend_token_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertEqual(
            response.data[0].code, 'phone_already_verified',
            response.data
        )

    def test_resend_token_phone_not_registered(self):
        resend_token_data = {
            'phone': '+628152121215',
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_verify_token_resend'], resend_token_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertEqual(
            response.data[0].code, 'phone_not_registered',
            response.data
        )

    def test_verify_phone_success(self):
        register_data = self.test_register_success()
        user = User.objects.get(phone=register_data['phone'])
        verify_phone_data = {
            'phone': register_data['phone'],
            'phone_token': user.phone_verification_token.token,
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_verify_phone'], verify_phone_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        # response should not send back token
        self.assertRaises(KeyError, lambda: response.data['phone_token'])
        self.assertIsNotNone(User.objects.get(phone=register_data['phone']).phone_verified)
        return register_data

    def test_verify_phone_already_verified(self):
        from social_network.models.verification import UserPhoneVerificationToken
        token = '000000'
        UserPhoneVerificationToken.objects.create(user=self.object_users[0], token=token)
        verify_phone_data = {
            'phone': str(self.object_users[0].phone),
            'phone_token': token,
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_verify_phone'], verify_phone_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        # response should not send back token
        self.assertRaises(TypeError, lambda: response.data['phone_token'])
        self.assertEqual(
            response.data[0].code, 'phone_already_verified',
            response.data
        )

    def test_verify_phone_not_registered(self):
        from social_network.models.verification import UserPhoneVerificationToken
        token = '000000'
        UserPhoneVerificationToken.objects.create(user=self.object_users[0], token=token)
        verify_phone_data = {
            'phone': '+628152121215',
            'phone_token': token,
        }
        response = self.client_anonymous.post(self.reversed_urls['registration_verify_phone'], verify_phone_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        # response should not send back token
        self.assertRaises(TypeError, lambda: response.data['phone_token'])
        self.assertEqual(
            response.data[0].code, 'phone_not_registered',
            response.data
        )

    def test_login_success(self):
        self.login_all_prepared_user()

    def test_login_after_register_success(self):
        register_data = self.test_verify_phone_success()
        data_login = {
            'username': register_data['phone'] or register_data['email'], 'password': register_data['password']
        }
        response = self.client_anonymous.post(self.reversed_urls['login_user'], data_login)
        token = response.data.get("token")
        self.assertNotEqual(token, None)
        return token

    def test_login_fail(self):
        data_login = {
            'username': self.data_users[0]['phone'] or self.data_users[0]['email'],
            'password': '100%invalid_password'
        }
        response = self.client_anonymous.post(self.reversed_urls['login_user'], data_login)
        self.assertRaises(KeyError, lambda: response.data['token'])
        # response should send back login error message
        assert response.data[api_settings.NON_FIELD_ERRORS_KEY]

    def test_login_before_registration_complete(self):
        register_data = self.test_register_success()
        data_login = {
            'username': register_data['phone'] or register_data['email'],
            'password': register_data['password']
        }
        response = self.client_anonymous.post(self.reversed_urls['login_user'], data_login)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, response.data)
        self.assertRaises(KeyError, lambda: response.data['token'])
        self.assertEqual(response.data[api_settings.NON_FIELD_ERRORS_KEY][0].code, 'registration_incomplete',
                         response.data)

    def test_logout(self):
        from rest_framework.test import APIClient
        token = self.test_login_after_register_success()
        api_client = APIClient(enforce_csrf_checks=True)
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token)
        response = api_client.post(self.reversed_urls['logout_user'])
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
