from django.urls import reverse
from rest_framework import status

from unit_tests.tests.common import PreparedTestCase


class NotificationTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()

    def test_read_all_notification(self):
        response = self.client_users[0].get(reverse('notification-personal'))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
