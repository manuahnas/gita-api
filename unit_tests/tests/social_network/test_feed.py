from rest_framework import status

from unit_tests.tests.common import PreparedTestCase


class FeedTest(PreparedTestCase):
    def test_feed_user(self):
        self.login_all_prepared_user()
        # TODO: Data still empty
        response = self.client_users[0].get(self.reversed_urls['feed'])
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_feed_unauthenticated(self):
        response = self.client_anonymous.get(self.reversed_urls['feed'])
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED, response.data)
