from unittest import skip

from django.db.models import Q
from django.urls import reverse
from rest_framework import status

from social_network.models import GroupProfile, GroupMembers
from unit_tests.tests.common import PreparedTestCase


class GroupTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()

    def test_search_group(self):
        # valid search
        group_to_search = 'Someone'
        response = self.client_users[0].get('{}?search={}'.format(reverse('group_search'), group_to_search))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            len(response.data['results']),
            GroupProfile.objects.filter(group_name__icontains=group_to_search).count()
        )
        # invalid search will return none group
        group_to_search = 'Invalid'
        response = self.client_users[0].get('{}?search={}'.format(reverse('group_search'), group_to_search))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(len(response.data['results']), 0)

    def test_create_group(self):
        group_data = {
            'group_name': 'New Group',
            'about': 'testing new group',
        }
        response = self.client_users[0].post(reverse('group_create'), group_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(
            response.data['group_name'],
            group_data['group_name']
        )
        self.assertEqual(
            GroupProfile.objects.count(),
            len(self.object_groups) + 1
        )

    def test_retrieve_group(self):
        response = self.client_users[0].get(
            reverse('group_detail', kwargs={'group_id': self.object_groups[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            response.data['group_name'],
            self.object_groups[0].group_name
        )

    def test_update_group(self):
        new_group_data = {
            'group_name': 'Updated Group',
            'about': 'testing update group',
        }
        # user not admin of the group
        response = self.client_users[5].patch(
            reverse('group_manage', kwargs={'group_id': self.object_groups[0].id}),
            new_group_data
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)
        self.assertNotEqual(
            new_group_data['group_name'],
            GroupProfile.objects.get(id=self.object_groups[0].id).group_name
        )
        # user is admin of the group
        response = self.client_users[0].patch(
            reverse('group_manage', kwargs={'group_id': self.object_groups[0].id}),
            new_group_data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            new_group_data['group_name'],
            GroupProfile.objects.get(id=self.object_groups[0].id).group_name
        )

    def test_delete_group(self):
        # admin of the group could not delete
        response = self.client_users[1].delete(
            reverse('group_manage', kwargs={'group_id': self.object_groups[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)
        # creator of the group could delete
        response = self.client_users[0].delete(
            reverse('group_manage', kwargs={'group_id': self.object_groups[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

    def test_all_member(self):
        response = self.client_users[0].get(
            reverse('group_members', kwargs={'group_id': self.object_groups[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            response.data['count'],
            self.object_groups[0].members.all().count()
        )

    def test_search_member(self):
        # valid search
        member_to_search = 'Dimas'
        response = self.client_users[0].get(
            '{}?search={}'.format(
                reverse('group_members', kwargs={'group_id': self.object_groups[0].id}),
                member_to_search
            )
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            len(response.data['results']),
            self.object_groups[0].members.filter(
                Q(member__first_name__icontains=member_to_search) |
                Q(member__last_name__icontains=member_to_search) |
                Q(member__username__icontains=member_to_search)
            ).count()
        )
        # invalid search will return empty result
        member_to_search = 'Invalid'
        response = self.client_users[0].get(
            '{}?search={}'.format(
                reverse('group_members', kwargs={'group_id': self.object_groups[0].id}),
                member_to_search
            )
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(len(response.data['results']), 0)

    def test_add_member(self):
        new_member_data = {
            'member': self.object_users[-1].id,
        }
        member_count_before = self.object_groups[0].members.all().count()
        response = self.client_users[0].post(
            reverse('group_add-member', kwargs={'group_id': self.object_groups[0].id}),
            new_member_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(
            response.data['member'],
            new_member_data['member']
        )
        self.assertEqual(
            self.object_groups[0].members.all().count(),
            member_count_before + 1
        )

    def test_remove_member(self):
        to_remove_member_data = {
            'member': GroupMembers.objects.filter(
                group__pk=self.object_groups[0].id,
                admin__isnull=True
            ).last().member.pk,
        }
        member_count_before = self.object_groups[0].members.all().count()
        response = self.client_users[0].delete(
            reverse('group_remove-member', kwargs={'group_id': self.object_groups[0].id}),
            to_remove_member_data
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)
        self.assertEqual(
            self.object_groups[0].members.all().count(),
            member_count_before - 1
        )

    @skip('TODO: promote admin endpoint')
    def test_promote_admin(self):
        # group_admin_promote
        pass

    @skip('TODO: promote admin endpoint')
    def test_demote_admin(self):
        # group_admin_demote
        pass
