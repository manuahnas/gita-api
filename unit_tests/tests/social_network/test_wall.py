from django.urls import reverse
from rest_framework import status

from social_network.models import WallPost, GroupWallPost, LocationWallPost
from unit_tests.tests.common import PreparedTestCase


class WallTest(PreparedTestCase):
    def setUp(self):
        self.login_all_prepared_user()

    def test_write_personal_wall(self):
        write_wall_data = {
            'body': 'Hello world!',
        }
        response = self.client_users[2].post(reverse('wall-user', kwargs={'user_id': 0}), write_wall_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(
            response.data['body'],
            write_wall_data['body']
        )
        self.assertEqual(
            WallPost.objects.filter(wall_of_user=self.object_users[2]).count(),
            1
        )

    def test_read_personal_wall(self):
        self.test_write_personal_wall()
        response = self.client_users[2].get(reverse('wall-user', kwargs={'user_id': 0}))
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            WallPost.objects.filter(wall_of_user=self.object_users[2]).count(),
            response.data['count']
        )
        self.assertEqual(
            response.data['results'][0]['body'],
            WallPost.objects.filter(wall_of_user=self.object_users[2])[0].body
        )
        return response.data

    def test_read_personal_wall_detail(self):
        wall_list = self.test_read_personal_wall()
        post_id = wall_list['results'][0]['id']
        response = self.client_users[2].get(
            reverse('wall-user_detail', kwargs={'post_id': post_id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            WallPost.objects.get(id=post_id).body,
            response.data['body']
        )

    def test_comment_personal_wall(self):
        write_wall_data = {
            'body': 'Hello world!',
        }
        response = self.client_users[2].post(reverse('wall-user', kwargs={'user_id': 0}), write_wall_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(
            response.data['body'],
            write_wall_data['body']
        )

    def test_like_personal_wall(self):
        pass

    def test_write_visit_wall(self):
        write_wall_data = {
            'body': 'Hello world!',
        }
        response = self.client_users[0].post(
            reverse('wall-user', kwargs={'user_id': self.object_users[1].id}),
            write_wall_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(
            response.data['body'],
            write_wall_data['body']
        )
        self.assertEqual(
            WallPost.objects.filter(wall_of_user=self.object_users[1]).count(),
            1
        )

    def test_read_visit_wall(self):
        self.test_write_visit_wall()
        response = self.client_users[0].get(
            reverse('wall-user', kwargs={'user_id': self.object_users[1].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            WallPost.objects.filter(wall_of_user=self.object_users[1]).count(),
            response.data['count']
        )
        self.assertEqual(
            response.data['results'][0]['body'],
            WallPost.objects.filter(wall_of_user=self.object_users[1])[0].body
        )
        return response.data

    def test_read_visit_wall_detail(self):
        wall_list = self.test_read_visit_wall()
        post_id = wall_list['results'][0]['id']
        response = self.client_users[0].get(
            reverse('wall-user_detail', kwargs={'post_id': post_id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            WallPost.objects.get(id=post_id).body,
            response.data['body']
        )

    def test_comment_visit_wall(self):
        pass

    def test_like_visit_wall(self):
        pass

    def test_write_group_wall(self):
        write_wall_data = {
            'body': 'Hello world!',
        }
        response = self.client_users[2].post(
            reverse('wall-group', kwargs={'group_id': self.object_groups[0].id}),
            write_wall_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(
            response.data['body'],
            write_wall_data['body']
        )
        self.assertEqual(
            GroupWallPost.objects.filter(wall_of_group=self.object_groups[0]).count(),
            1
        )

    def test_read_group_wall(self):
        self.test_write_group_wall()
        response = self.client_users[2].get(
            reverse('wall-group', kwargs={'group_id': self.object_groups[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            GroupWallPost.objects.filter(wall_of_group=self.object_groups[0]).count(),
            response.data['count']
        )
        self.assertEqual(
            response.data['results'][0]['body'],
            GroupWallPost.objects.filter(wall_of_group=self.object_groups[0])[0].body
        )
        return response.data

    def test_read_group_wall_detail(self):
        wall_list = self.test_read_group_wall()
        post_id = wall_list['results'][0]['id']
        response = self.client_users[2].get(
            reverse('wall-group_detail', kwargs={'post_id': post_id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            GroupWallPost.objects.get(id=post_id).body,
            response.data['body']
        )

    def test_comment_group_wall(self):
        pass

    def test_like_group_wall(self):
        pass

    def test_write_location_wall(self):
        write_wall_data = {
            'body': 'Hello world!',
        }
        response = self.client_users[2].post(
            reverse('wall-location', kwargs={'location_id': self.object_locations[0].id}),
            write_wall_data
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(
            response.data['body'],
            write_wall_data['body']
        )
        self.assertEqual(
            LocationWallPost.objects.filter(wall_of_location=self.object_locations[0]).count(),
            1
        )

    def test_read_location_wall(self):
        self.test_write_location_wall()
        response = self.client_users[2].get(
            reverse('wall-location', kwargs={'location_id': self.object_locations[0].id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            LocationWallPost.objects.filter(wall_of_location=self.object_locations[0]).count(),
            response.data['count']
        )
        self.assertEqual(
            response.data['results'][0]['body'],
            LocationWallPost.objects.filter(wall_of_location=self.object_locations[0])[0].body
        )
        return response.data

    def test_read_location_wall_detail(self):
        wall_list = self.test_read_location_wall()
        post_id = wall_list['results'][0]['id']
        response = self.client_users[2].get(
            reverse('wall-location_detail', kwargs={'post_id': post_id})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(
            LocationWallPost.objects.get(id=post_id).body,
            response.data['body']
        )

    def test_comment_location_wall(self):
        pass

    def test_like_location_wall(self):
        pass
